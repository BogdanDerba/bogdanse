package com.profit.bogdan;

import com.profit.bogdan.inheritance.Circle;
import com.profit.bogdan.inheritance.Cylindre;
import org.junit.Test;

/**
 * Created by profit on 11.06.17.
 */
public class Figure {
    @Test
    public void getArea() {

        Circle circle = new Circle();
        System.out.println(circle.getArea());
        System.out.println(circle.toString());

        Circle circle1 = new Circle(4.2);
        System.out.println(circle1.getArea());
    }
    @Test
    public void  cylindrData() {
        System.out.println("-------------------------------");
        Cylindre cylindre = new Cylindre();
        System.out.println(cylindre.getHeight());
        System.out.println(cylindre.getArea());
        System.out.println(cylindre.getRadius());
        System.out.println(cylindre.toString());
        System.out.println(cylindre.getVolume());
        System.out.println("-------------------------------");
       // Cylindre cylindre1 = new Cylindre(3.4);
        //System.out.println(cylindre1.getHeight());
       // System.out.println(cylindre1.getVolume());
        //System.out.println(cylindre1.toString());


        System.out.println("-------------------------------");
        Cylindre cylindre2 = new Cylindre(4.5, 5.6);
        System.out.println(cylindre2.getRadius());
        System.out.println(cylindre2.getHeight());
        System.out.println(cylindre2.getArea());
        System.out.println(cylindre2.getVolume());
        System.out.println(cylindre2.toString() );

    }


    }