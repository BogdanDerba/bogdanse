package com.profit.bogdan.stringTest;

import static com.profit.bogdan.firstapp.FirstApp.*;

import com.profit.bogdan.strings.Converter;
import com.profit.bogdan.strings.ReadFile;
import com.profit.bogdan.strings.SubString;
import com.profit.bogdan.strings.StringTokenazing;
import org.junit.Test;
import org.xml.sax.SAXException;
;import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by profit on 10.06.17.
 */
public class SubStringTest {
    @Test
    public void subTest() {

        String str = "rtnerdstgerywgwdsfgntr";
        print(SubString.subStringSearch(str));

    }

    @Test
    public void stringTokenTest() {

        String str = "gd , serg, sfg";
        StringTokenazing.splitBySpace(str);

    }

    @Test
    public void stringTokenComaTest() {

        String str = "gd , serg, sfg";
        StringTokenazing.splitByComma(str);

    }

    @Test
    public void stringTokenDefisTest() {

        String str = "gd - serg - sfg";
        StringTokenazing.splitByDefis(str);

    }


    @Test
    public void fileCsvTest() {

        String path = "/home/profit/IdeaProjects/bogdan/src/main/resources/csv/file.csv";
        ReadFile.readCsv(path);

    }


    @Test
    public void fileBufferReaderTest() throws IOException, SAXException, ParserConfigurationException, TransformerException {

        String x = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                "<ListDomainsResponse xmlns=\"http://sdb.amazonaws.com/doc/2009-04-15/\">\n" +
                "    <ListDomainsResult>\n" +
                "        <DomainName>Audio</DomainName>\n" +
                "        <DomainName>Course</DomainName>\n" +
                "        <DomainName>DocumentContents</DomainName>\n" +
                "        <DomainName>LectureSet</DomainName>\n" +
                "        <DomainName>MetaData</DomainName>\n" +
                "        <DomainName>Professors</DomainName>\n" +
                "        <DomainName>Tag</DomainName>\n" +
                "    </ListDomainsResult>\n" +
                "    <ResponseMetadata>\n" +
                "        <RequestId>42330b4a-e134-6aec-e62a-5869ac2b4575</RequestId>\n" +
                "        <BoxUsage>0.0000071759</BoxUsage>\n" +
                "    </ResponseMetadata>\n" +
                "</ListDomainsResponse>";

        Converter converter = new Converter();
        System.out.println(converter.documentToString(Converter.stringToDom(x)));


        Converter.stringToDomFile(x);
    }
    @Test
    public void readXml() throws IOException, ParserConfigurationException, SAXException{
        System.out.println(Converter.lineXml("/home/profit/IdeaProjects/bogdan/my-file.xml"));
        //System.out.println(Converter.stringToDom(Converter.lineXml(" ").toString()));

    }
}
