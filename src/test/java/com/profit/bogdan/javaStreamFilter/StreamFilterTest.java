package com.profit.bogdan.javaStreamFilter;

import com.profit.bogdan.javaFilter.Person;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by profit on 30.07.17.
 */
public class StreamFilterTest {

    @Test
    public void  filter(){

        List<String> lines = Arrays.asList("first","second", "third");

        List<String> result = lines.stream().filter(line-> !"second".equals(line)).collect(Collectors.toList());

        result.forEach(System.out::println);
    }

    @Test
    public void filterSecond(){
        List<Person> person = Arrays.asList(
          new Person(1,"first",30),
        new Person(2,"second",20),
        new Person(3,"third",40)

        );

        Person result = person.stream()
                .filter(x-> x.getId().equals(2)).findAny().orElse(null);
        System.out.println(result);


        Person result2 = person.stream()
                .filter(x-> x.getId().equals(5)).findAny().orElse(null);
        System.out.println(result2);

        Person result3 = person.stream()
                .filter(x-> x.getId().equals(1)).findAny().orElse(null);
        System.out.println(result3);
    }

    @Test
    public void filterAndMap() {
        List<Person> person = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 20),
                new Person(3, "third", 40)

        );


        String name = person.stream()
                .filter(x->"second".equals(x.getName()))
                .map(Person::getName)
                .findAny()
                .orElse(" ");

        System.out.println("name : " + name);

        System.out.println("77777777777");

        List<String> collect = person.stream()
                .map(Person:: getName)
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
        System.out.println("9999999999999999");

        Integer id = person.stream().filter(x->x.getId().equals(3))
                .map(Person::getId)
                .findAny()
                .orElse(null);

        System.out.println("id : " + id);


        Person person1 = person.stream().filter(x-> x.getId().equals(id))
                .findAny().orElse(null);

        System.out.println(person1);


        List<Integer> collectId = person.stream().map(Person :: getId).collect(Collectors.toList());

        collectId.forEach(System.out::println);
//////////////////////////

        System.out.println("//////////////////////////");

        List<Person> collect1 = person.stream().collect(Collectors.toList());
        collect1.forEach(System.out::println);

        System.out.println("//////////////////////////");
        List<String> collect2 = person.stream().map(Person::toString).collect(Collectors.toList());
        collect2.forEach(System.out::println);
        System.out.println("//////////////////////////");

        collect2.forEach(x->{
           System.out.println(x);
        });


        System.out.println("//////////////////////////");

        collect2.forEach(System.out::println);

    }

    @Test
    public void map(){

        List<String> alpha = Arrays.asList("a", "b", "c","d");
//Before Java 8
        List<String> alphaUpper = new ArrayList<>();

        for (String s : alpha){
            alphaUpper.add(s.toUpperCase());
        }

        System.out.println(alpha);//[a  b  c  d]

        System.out.println(alphaUpper);//[A  B  C  D]

//Java 8
        List<String> collect = alpha.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(collect);//A  B  C  D
//Extra, streams apply to any data type
        List<Integer> num = Arrays.asList(1,2,3,4,5,6,7,8);

        List<Integer> collect1 = num.stream().map(n-> n * (int) (Math.random() *100)).collect(Collectors.toList());
        System.out.println(collect1);
    }


    @Test
    public void mapObject() {

        List<Person> staff = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 20),
                new Person(3, "third", 40)


        );


        //before j8

        List<String> result = new ArrayList<>();

        for (Person x : staff
                ) {
            result.add(x.getName());


        }
        System.out.println(result);

        //after j8

        List<String> collect = staff.stream().map(x -> x.getName()).collect(Collectors.toList());

        System.out.println(collect);

        ///

        List<Person> result2 = staff.stream().map(temp -> {
            Person obj = new Person();
            obj.setName(temp.getName());
            obj.setAge(temp.getAge());
            return obj;
        }).collect(Collectors.toList());
        System.out.println(result2);
    }
        @Test
                public void groupingBy(){
            List<String> items = Arrays.asList("apple","apple","orange", "banana","apple"
            ,"banana", "papaya");

            Map<String, Long> result3 = items.stream()
                    .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));

            System.out.println(result3);

            Map<String, Long> finalMap = new LinkedHashMap<>();

            result3.entrySet().stream().sorted(Map.Entry.<String, Long> comparingByKey().reversed())
                    .forEachOrdered(e-> finalMap.put(e.getKey(),e.getValue()));

            System.out.println(finalMap);

            Stream.of("AAA", "BBB", "CCC").parallel().forEach(s-> System.out.print("Output : " + s + " " ));
            System.out.println("\n");
            Stream.of("AAA", "BBB", "CCC").parallel().forEachOrdered(s-> System.out.print("Output : " + s + " "));
        }

        @Test
    public void filterForNull(){

            Supplier<Stream<String>> langguageSuppler = () -> Stream.of("java","python","nude", null, "ruby",null);

            List<String> result = langguageSuppler.get().filter(x->x!=null).collect(Collectors.toList());

            result.forEach(System.out::println);

            System.out.println("-----------------------");

            List<String> resltNew = langguageSuppler.get().filter(Objects::nonNull).collect(Collectors.toList());

                resltNew.forEach(System.out::println);

            Stream.of(langguageSuppler).parallel().forEachOrdered(s-> System.out.print("Output : " + resltNew+ " "));
            Stream.of(langguageSuppler).parallel().forEach(s-> System.out.print("Output : " + resltNew+ " "));
                }

@Test
    public void arrayToStream(){
        String[] array = {"a","b", "c","d"};

        Stream<String> stream1 = Arrays.stream(array);
        stream1.forEach(x -> System.out.println(x));


    Stream<String> stream2 = Arrays.stream(array);
    stream2.forEach( System.out::println);

}


@Test
    public void primitiveArrayToStream(){

        int[] inArray = {1,2,3,4,5,6};

    IntStream intStream1 = Arrays.stream(inArray);
    intStream1.forEach(System.out::println);

    Stream<int[]> temp =Stream.of(inArray);


    IntStream intStream2 = temp.flatMapToInt(x -> Arrays.stream(x));
    intStream2.forEach(System.out::println);


}

@Test
    public void convertStreamToList(){

        Stream<String> language = Stream.of("java","python","nude");

        List<String> result = language.collect(Collectors.toList());

        result.forEach(System.out::println);

        Stream<Integer> number = Stream.of(1, 2, 3, 5, 7, 9);

        List<Integer> result2 = number.filter(x->x!=3).collect(Collectors.toList());
    result2.forEach(System.out::println);

}

@Test
public  void pre(){
    List<Person> staff = Arrays.asList(
            new Person(1, "first", 30),
            new Person(2, "second", 20),
            new Person(3, "third", 40)

    );

    Map<Integer, String> map = new HashMap<>();

    map = mapObj(staff);

    map.forEach((x, y) -> {
        System.out.println(x + " " + y);
    });

    map = staff.stream().collect(Collectors.toMap(Person::getId,Person::getName));

    map.forEach((x,y) -> {
        System.out.println(x + " " + y);

    });
}

public HashMap<Integer, String> mapObj(List<Person> personList){

    HashMap<Integer, String> map = new HashMap<>();
    personList.forEach(x -> {
        map.put(x.getId(),x.getName());
    });
    return map;
}



    }


