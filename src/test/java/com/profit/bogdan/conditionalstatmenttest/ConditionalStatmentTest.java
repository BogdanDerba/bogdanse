package com.profit.bogdan.conditionalstatmenttest;

import org.junit.Test;
import static com.profit.bogdan.conditional.ConditionalStatements.positiveOrNegative;

/**
 * Created by profit on 28.05.17.
 */
public class ConditionalStatmentTest {

    @Test
    public void conditionalTest() {
        positiveOrNegative(4);
        positiveOrNegative(-3);
        positiveOrNegative(0);

    }
}
