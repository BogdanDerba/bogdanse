package com.profit.bogdan.exeptionTest;

import com.profit.bogdan.encapsulation.CustomerService;
import com.profit.bogdan.exception.NameNotFoundException;
import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.hamcrest.CustomMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.Rule;
import org.junit.rules.ExpectedException;


/**
 * Created by profit on 02.07.17.
 */
/*@RunWith(HierarchicalContextRunner.class)
public class ExceptionTest {
    @Test(expected = ArithmeticException.class)
    public void
    testDivisionWithException() {
        int i = 1 / 0;
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyList() {
        new ArrayList<>().get(0);
    }

    public class ExceptionTest2 {

        @Test
        public void testDivisionWithException() {
            try {
                int i = 1 / 0;
                fail();//remember this line, else "may" false positive;
            } catch (ArithmeticException e) {
                assertThat(e.getMessage(), is("/ by zero"));
                System.out.println(e.getMessage());
            }

        }

        @Test
        public void testEmptyList() {
            try {
                new ArrayList<>().get(0);
                fail();
            } catch (IndexOutOfBoundsException e) {
                assertThat(e.getMessage(), is("Index: 0, Size: 0"));
                System.out.println(e.getMessage());
            }
        }


        public class ExceptinTest3 {

            @Rule
            public ExpectedException thrown = ExpectedException.none();


            @Test
            public void testDevisionWithException() {

                thrown.expect(ArithmeticException.class);
                thrown.expectMessage(containsString("/ by zero"));

                int i = 1 / 0;
            }

            @Test
            public void testNameFoundException() throws NameNotFoundException{
                //
                thrown.expect(NameNotFoundException.class);

                //
                thrown.expectMessage(is("Name is empty!"));

                //test detail
                thrown.expect(hasProperty("errCode"));
                thrown.expect(hasProperty("errCode", is(666)));

              CustomerService cust = new CustomerService();
              cust.findByName("");
            }
        }


    }

}*/
