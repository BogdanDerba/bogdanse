package com.profit.bogdan.polimorphism;

import com.profit.bogdan.polymorphism.Staff;
import org.junit.Test;

/**
 * Created by profit on 17.06.17.
 */
public class Firm {
    @Test
    public void poly(){
        Staff personnel = new Staff();
        personnel.payDay();
    }
}
