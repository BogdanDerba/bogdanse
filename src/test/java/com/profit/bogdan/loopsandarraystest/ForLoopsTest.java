package com.profit.bogdan.loopsandarraystest;

import com.profit.bogdan.loopsandarrays.Sorting;
import org.junit.Test;

import java.util.Arrays;

import static com.profit.bogdan.loopsandarrays.ForLoops.*;
import static com.profit.bogdan.loopsandarrays.Matrix.*;
import static com.profit.bogdan.loopsandarrays.Sorting.*;
import static com.profit.bogdan.loopsandarrays.Sorting.bubbleSort;

/**
 * Created by profit on 03.06.17.
 */
public class ForLoopsTest {
    @Test
    public void forLoopTest() {

        int[] x = {5, 6, 7, 8};
        int[] y = {5, 6, 7, 8};
        int[] q = {5, 6, 7, 9};

        equalityOfTwoArrays(x, y);
        equalityOfTwoArrays(y, q);

    }

    @Test
    public void forLoopTestO() {
        int[] x = {5, 6, 7, 8};
        int[] u = {5, 6, 7, 8, 8, 54, 5, 61};

        uniqueArray(x);
        uniqueArray(u);
    }

    @Test
    public void forMAtrix() {

        Double[][] e = {{3.4, 5.6, 6.8, 7.7},
                {4.3, 5.5, 6.6, 7.7},
                {4.4, 6.7, 6.6, 6.7},
                {4.4, 6.7, 6.6, 6.7}};
        Double[][] r = {{3.4, 5.6, 6.8, 7.7},
                {4.3, 5.5, 6.6, 7.7},
                {4.4, 6.7, 6.6, 6.7},
                {4.4, 6.7, 6.6, 6.7}};
        Double[][] result = multiplicar(e, r);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result.length; j++) {
                System.out.println(result[i][j] + " ");

            }
            System.out.println();
        }

    }
    @Test
    public void forSorting(){

        int[] num = {2, 1, 4, 3};
        selectionSort(num);
        System.out.println(Arrays.toString(num));
    }
        @Test
    public void forSortingTwo() {
            int[] num = {2, 1, 4, 3};
            selectionSort(num);
            for (int i = 0; i < num.length; i++) {
                System.out.print(num[i] + " ");

            }
        }



    @Test
    public void  bubbleSort(){

        int[] array = {2, 1, 4, 3};
        Sorting.bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }

    @Test
    public void forSortingTwotwo() {
        int[] num = {2, 1, 4, 3, 5, 6,345, 3465, 3456, 345, 3, };
        Sorting.bubbleSort(num);
        for (int i = 0; i < num.length; i++) {
            System.out.print(num[i] + " ");

        }
    }

}
