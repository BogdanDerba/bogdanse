package com.profit.bogdan.collectionsTest;

import com.profit.bogdan.collections.FruitIterator;
import com.profit.bogdan.collections.MyOwnArrayList;
import com.profit.bogdan.reflection.Test;

/**
 * Created by profit on 09.07.17.
 */
public class MyFruitIteratorExample {
    @org.junit.Test
    public void test(){
        MyOwnArrayList fruitList = new MyOwnArrayList();
        fruitList.add("Mango");
        fruitList.add("Strawberry");
        fruitList.add("Watermalon");
        fruitList.add("Papaya");

        System.out.println("-------Calling my  iterator on my Array---------");


        FruitIterator it = fruitList.iterator();
        while (it.hasNext()){
            String s = (String)it.next();
            System.out.println("Value: " + s);
        }
        System.out.println("--Fruit List size: " + fruitList.size());
        fruitList.remove(1);
        System.out.println("---After removal, Fruit List size: " + fruitList.size());

        for (int i = 0; i <fruitList.size() ; i++) {
            System.out.println(fruitList.get(i));

        }

    }
}
