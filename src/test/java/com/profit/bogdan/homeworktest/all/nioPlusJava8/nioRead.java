package com.profit.bogdan.homeworktest.all.nioPlusJava8;


import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Stream;

import static java.lang.System.out;

/**
 * Created by bogdan on 02.08.17.
 */
public class nioRead {


    @Test
    public void readText() {


        long wordCount = 0;
        Path textFilePath = Paths.get("/home/bogdan/IdeaProjects/bogdan/target/teslaModel3.txt");


        try {

            Stream<String> fileLines = Files.lines(textFilePath, Charset.defaultCharset());
            wordCount = fileLines.flatMap(line -> Arrays.stream(line.split(" "))).count();


        } catch (IOException e) {
            e.printStackTrace();
        }
        out.println("count  " + wordCount);
    }

   }