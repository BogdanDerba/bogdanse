package com.profit.bogdan.homeworktest.all.linkListTest;

import com.profit.bogdan.homework.linklisttask.linklistfirst.LinkList;
import com.profit.bogdan.homework.linklisttask.linklistsecond.DoubleEndedLinkedList;
import com.profit.bogdan.homework.linklisttask.linklistsecond.NeighborIterator;
import org.junit.Test;

/**
 * Created by bogdan on 10.07.17.
 */
public class LinkListTest {
    @Test
    public  void linkTest(){
        LinkList theLinkedList = new LinkList();

        theLinkedList.insertFirstLink("Danny Boil",34);
        theLinkedList.insertFirstLink("War & Peace",12);
        theLinkedList.insertFirstLink("Harry Potter ...",67);
        theLinkedList.insertFirstLink("Warrior Way",24);
        theLinkedList.insertFirstLink("Game of trons",32);
        theLinkedList.insertFirstLink("Hobbit",50);

        theLinkedList.display();

        System.out.println("===========================================");

        theLinkedList.removeFirst();

        System.out.println("List after removeng first book: \n");

        theLinkedList.display();

    }

    @Test
    public void  doubleEndedLinkedListTest(){

        DoubleEndedLinkedList theLinkedList = new DoubleEndedLinkedList();

        theLinkedList.insertInOrder("Leon",23);
        theLinkedList.insertInOrder("Daria",32);
        theLinkedList.insertInOrder("Sam",2);
        theLinkedList.insertInOrder("Ihor",4);
        theLinkedList.insertInOrder("Alex",52);
        theLinkedList.insertInOrder("Andy",57);
        theLinkedList.insertInOrder("Zoi",22);
        theLinkedList.insertInOrder("Clark",48);
        theLinkedList.insertInOrder("Bogdan",13);

        theLinkedList.display();
        System.out.println("===========================================");
        theLinkedList.insertAfterKey("BogdanWithKey",1, 5);
        theLinkedList.insertAfterKey("ZoiWithKey",22, 6);
        theLinkedList.insertAfterKey("AlexWithKey",52, 7);

        theLinkedList.display();


        NeighborIterator neighbors = new NeighborIterator(theLinkedList);
        System.out.println("=======DISPLAY========");
       neighbors.currentNeighbor.display();

       System.out.println(neighbors.hasNext());
        neighbors.next();
        System.out.println("=======DISPLAY========");
        System.out.println("=======HAS NEXT========");
        neighbors.currentNeighbor.display();

        neighbors.remove();
        System.out.println("=======DISPLAY========");
        System.out.println("=======REMOVE========");
        neighbors.currentNeighbor.display();

    }
}
