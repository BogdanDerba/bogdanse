package com.profit.bogdan.homeworktest.all.homeworkzero;

import org.junit.Test;

import static com.profit.bogdan.homework.taskzero.TaskTwo.*;
/**
 * Created by bogdan on 02.06.17.
 */
public class HomeWorkTestTwo {

    @Test
    public void simpleSortForTwoDigitsTest() {

        simpleSortForTwoDigits(3, 2);
        simpleSortForTwoDigits(3, 5);
        simpleSortForTwoDigits(31, 0);

    }

    @Test
    public void simpleSortForTreeDigitsTest() {

        simpleSortForTreeDigits(3, 2,1);
        simpleSortForTreeDigits(3, 5,7);
        simpleSortForTreeDigits(31, 0,3);

    }

    @Test
    public void simpleSortForFourDigitsTest() {

        simpleSortForFourDigit(3, 234,235,231);
        simpleSortForFourDigit(46,346,1,98);
        simpleSortForFourDigit(765,6,24,1);

    }
}