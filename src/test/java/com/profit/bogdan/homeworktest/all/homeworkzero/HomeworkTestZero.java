package com.profit.bogdan.homeworktest.all.homeworkzero;

import org.junit.Test;

import static com.profit.bogdan.homework.taskzero.TaskZero.maxDigitOfThree;
import static com.profit.bogdan.homework.taskzero.TaskZeroTwo.maxDigitOfFour;
/**
 * Created by bogdan on 01.06.17.
 */
public class HomeworkTestZero {

    @Test
    public void MaxDigit() {
        maxDigitOfThree(1, 2, 3);

    }

    @Test
    public void MaxDigitOne() {
        maxDigitOfFour(101, 7, 9,3);

    }
}
