package com.profit.bogdan.homeworktest.all.homeworktwo;

import com.profit.bogdan.homework.tasktwo.figure.*;
import org.junit.Test;

/**
 * Created by bogdan on 15.06.17.
 */
public class HomeTaskFigureTest {

    @Test
    public void squareData() {
        Square square = new Square(0, 2.3, 1.2);
        System.out.println("The area of square is ");
        System.out.println(square.getArea());
        System.out.println("The perimetr of square is  ");
        System.out.println(square.getPerimetr());
    }

    @Test
    public void cubeData() {
        Cube cube = new Cube(0, 3, 3);
        System.out.println("The area of cube is ");
        System.out.println(cube.getArea());
    }

    @Test
    public void parallelogramData() {
        Parallelogram parallelogram = new Parallelogram(32.0, 23.0, 23.0);
        System.out.println("The area of Parallelogram is ");
        System.out.println(parallelogram.getArea());
        System.out.println("The perimetr of Parallelogram is  ");
        System.out.println(parallelogram.getPerimetr());
    }

    @Test
    public void isoTriangleData() {

        IsoscelesTriangle iso = new IsoscelesTriangle(0, 2.0, 2.0, 2.0);
        System.out.println("The area of Isosceles Triangle is ");
        System.out.println(iso.getArea());
        System.out.println("The perimetr of Isosceles Triangle is  ");
        System.out.println(iso.getPerimetr());
    }

    @Test
    public void rightTriangleData() {

        RightTriangle rightTriangle = new RightTriangle(0, 3.0, 2.0, 5.0);
        System.out.println("The area of Right Triangle is ");
        System.out.println(rightTriangle.getArea());
        System.out.println("The perimetr of Right Triangle is  ");
        System.out.println(rightTriangle.getPerimetr());
    }

    @Test
    public void circle() {

        Circle circle = new Circle(4.3);
        System.out.println("The area of Circle is ");
        System.out.println(circle.getArea());
    }

    @Test
    public void cylindre() {
        Cylindre cylindre = new Cylindre(4, 5);
        System.out.println("The area of Cylinder is ");
        System.out.println(cylindre.getArea());
    }
}
