package com.profit.bogdan.testPatterns;

import com.profit.bogdan.pattern.abstractFactory.AbstractFactory;
import com.profit.bogdan.pattern.abstractFactory.SpeciesFactory;
import com.profit.bogdan.pattern.builder.JapaneseMealBuilder;
import com.profit.bogdan.pattern.builder.Meal;
import com.profit.bogdan.pattern.builder.MealBuilder;
import com.profit.bogdan.pattern.builder.MealDirector;
import com.profit.bogdan.pattern.factory.Animal;
import com.profit.bogdan.pattern.factory.AnimalFactory;
import com.profit.bogdan.pattern.factory.Student;
import com.profit.bogdan.pattern.prototype.Dog;
import com.profit.bogdan.pattern.prototype.Person;
import com.profit.bogdan.pattern.singletone.SingletonExample;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by profit on 15.07.17.
 */
public class TestPatterns {
    @org.junit.Test
    public void builder(){

        Student s = new Student.Builder().name("Bogdan").age(26).language(Arrays.asList("ENG","RUS")).build();

        System.out.println(s);
    }

    @org.junit.Test
    public void singleton(){
        SingletonExample singletonExample = SingletonExample.getInstance();
        singletonExample.sayHello();
    }

    @Test
    public void animalFactoryTest(){

        AnimalFactory animalFactory = new AnimalFactory();

        Animal a1= animalFactory.getAnimal("feline");
        System.out.println("a1 sound : " + a1.makeSound());


        Animal a2= animalFactory.getAnimal("canine");
        System.out.println("a2 sound : " + a2.makeSound());

    }

    @Test
    public void abstractFactoryTest(){

        AbstractFactory abstractFactory= new AbstractFactory();

        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("Reptile");
        Animal a1 = speciesFactory1.getAnimal("tyrannosaurus");
        System.out.println("a1 sound : " + a1.makeSound());
        Animal a2 = speciesFactory1.getAnimal("snake");
        System.out.println("a2 sound : " + a2.makeSound());

        Animal animal = abstractFactory.getSpeciesFactory("Reptile").getAnimal("snake");
        System.out.println("reptile sound : " + animal.makeSound());


        SpeciesFactory speciesFactory2 = abstractFactory.getSpeciesFactory("mammal");
        Animal a3 = speciesFactory2.getAnimal("dog");
        System.out.println("a3 sound : " + a3.makeSound());
        Animal a4 = speciesFactory2.getAnimal("cat");
        System.out.println("a4 sound : " + a4.makeSound());


    }

    @Test
    public void prototype(){

        Person person1 = new Person("Frad");
        System.out.println("person 1: " + person1);
        Person person2 = (Person) person1.doClone();
        System.out.println("person 2: " + person2);

        Dog dog1 = new Dog("Woof!");
        System.out.println("Dog 1 =" + dog1);
        Dog dog2 = (Dog) dog1.doClone();
        System.out.println("Dog 2 =" + dog2);

    }

    @Test
    public void builder2(){

        MealBuilder mealBuilder = new JapaneseMealBuilder();

        MealDirector mealDirector = new MealDirector(mealBuilder);
        mealDirector.constractMeal();
        Meal meal = mealDirector.getMeal();

        System.out.println("meal is : " + meal);
/**
 * for more optim. we make MealDirector not void - becouse we return
 *
 */
        Meal x = new MealDirector(new JapaneseMealBuilder()).constractMeal().getMeal();
        System.out.println(x);

    }


}
