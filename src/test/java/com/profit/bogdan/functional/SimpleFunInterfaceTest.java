package com.profit.bogdan.functional;

import org.junit.Test;

import java.util.*;
import java.util.function.*;

/**
 * Created by aleksey on 28.07.17.
 */
public class SimpleFunInterfaceTest {

    @Test
    public void test(){

        checkWork(new SimpleFuncInterface() {
            @Override
            public void doWork() {
                System.out.println("Do work in SimpleFun impl...");
            }
        });

        checkWork(() -> System.out.println("Do work in lambda exp impl..."));

    }

    public static void checkWork(SimpleFuncInterface simpleFuncInterface) {
        simpleFuncInterface.doWork();

    }

    @Test
    public void predicateTest() {

        Predicate<Integer> isPositive = x -> x > 0;

        System.out.println(isPositive.test(5)); // true
        System.out.println(isPositive.test(-7)); // false


        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        System.out.println("Print all numbers:");

        //pass n as parameter
        eval(list, n->true);


        System.out.println("Print even numbers:");
        eval(list, n-> n%2 == 0 );

        System.out.println("Print numbers greater than 3:");
        eval(list, n-> n > 3 );
    }

    public static void eval(List<Integer> list, Predicate<Integer> predicate) {
        for(Integer n: list) {

            if(predicate.test(n)) {
                System.out.println(n + " ");
            }
        }
    }

    @Test
    public void binaryOperator() {

        BinaryOperator<Integer> multiply = (x, y) -> x*y;

        System.out.println(multiply.apply(3, 5)); // 15
        System.out.println(multiply.apply(10, -2)); // -20
    }

    @Test
    public void unaryOperator() {
        UnaryOperator<Integer> square = x -> x*x;
        System.out.println(square.apply(5)); // 25
    }

    @Test
    public void function() {
        Function<Integer, String> convert = x-> String.valueOf(x) + " euro";
        System.out.println(convert.apply(5));
    }

    @Test
    public void consumer() {
        Consumer<Integer> printer = x-> System.out.printf("%d euro \n", x);
        printer.accept(600);
    }

    @Test
    public void java8ForEachAndMAp() {

        Map<String, Integer> items = new HashMap<>();
        items.put("A", 10);
        items.put("B", 20);
        items.put("C", 30);
        items.put("D", 40);
        items.put("E", 50);
        items.put("F", 60);


        //regular
        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            System.out.println("Item : " + entry.getKey() + " Count : " + entry.getValue());
        }

        System.out.println("-------------------------------------------");

        //java 8
        items.forEach((k,v)->System.out.println("Item : " + k + " Count : " + v));

        items.forEach((k,v)->{
            System.out.println("Item : " + k + " Count : " + v);
            if("E".equals(k)){
                System.out.println("Hello E");
            }
        });
    }

    @Test
    public void java8ForEachAndList() {

        List<String> items = new ArrayList<>();
        items.add("A");
        items.add("B");
        items.add("C");
        items.add("D");
        items.add("E");

        //regular
        for(String item : items){
            System.out.println(item);
        }

        System.out.println("-------------------------------------------");

        //lambda
        //Output : A,B,C,D,E
        items.forEach(item->System.out.println(item));

        System.out.println("-------------------------------------------");

        //Output : C
        items.forEach(item->{
            if("C".equals(item)){
                System.out.println(item);
            }
        });

        System.out.println("-------------------------------------------");

        //method reference
        //Output : A,B,C,D,E
        items.forEach(System.out::println);

        System.out.println("-------------------------------------------");

        //Stream and filter
        //Output : B
        items.stream()
                .filter(s->s.contains("B"))
                .forEach(System.out::println);


    }


}
