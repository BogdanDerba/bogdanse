package com.profit.bogdan.nio;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by profit on 22.07.17.
 */
public class TestReadFile {


    @Test
    public void testRead(){
        String fileName= "/home/profit/IdeaProjects/bogdan/pom.xml";

        //
        try (Stream<String> stream = Files.lines(Paths.get(fileName))){

            stream.forEach(System.out:: println);

            }
            catch (IOException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testSecondReader() {

        String fileName= "/home/profit/IdeaProjects/bogdan/pom.xml";
        List<String> list = new ArrayList<>();

        //
        try (Stream<String> stream = Files.lines(Paths.get(fileName))){

            //
            //
            //

            list = stream
                    .filter(line -> line.startsWith("<"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());

        }
        catch (IOException e){
            e.printStackTrace();
        }
       list.forEach(System.out:: println);

    }

    @Test
    public void testReference(){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(3);
        arrayList.add(6);
        arrayList.add(9);
        arrayList.add(12);
        arrayList.add(15);
        arrayList.add(18);
        arrayList.add(21);
        arrayList.add(24);
        arrayList.add(27);

        arrayList.forEach(TestReadFile::square);


    }

    public static void square (int num){
        System.out.println(Math.pow(num, 2));
    }

    private static void convertPaths(){
        Path relative = Paths.get("paths.txt");
        System.out.println("Relative path: " + relative);
        Path absolute = relative.toAbsolutePath();
        System.out.println("Absolute path: " + absolute);
    }

   @Test
   public void paths() throws IOException{
        Path path = Paths.get("/home/profit/IdeaProjects/bogdan/paths.txt");
        String question = "To be or not to be ";

       Files.write(path, question.getBytes());


       Path path2 = Paths.get("/home/profit/IdeaProjects/bogdan/paths.txt");

       System.out.println(path2.getFileName()+
       " " + path2.getName(0) + " " +
       path.getNameCount() + " " +
       path2.subpath(0,2) + " " +
               path2.getParent() + " " +
       path2.getRoot());

       TestReadFile.convertPaths();
   }

   @Test
    public void forEachTest() throws Exception{

       List<String> list = new ArrayList<>();
       Path path = Paths.get("/home/profit/IdeaProjects/bogdan/paths.txt");



      list.add(String.valueOf(path.getFileName()));
      list.add(String.valueOf(path.getName(0)));
       list.add(String.valueOf(path.getNameCount()));
       list.add(String.valueOf(path.subpath(0,2)));
       list.add(String.valueOf(path.getParent()));
       list.add(String.valueOf(path.getRoot()));



       list.forEach(System.out:: println);

   }

   @Test
    public void copy(){

        Path oldFile = Paths.get("/home/profit/IdeaProjects/bogdan/","paths.txt");

        Path newFile = Paths.get("src/main/resources","newFile.txt");
        try(OutputStream os = new FileOutputStream(newFile.toFile())){
            Files.copy(oldFile,os);

        }
        catch (IOException ex){
            ex.printStackTrace();
        }
   }
}
