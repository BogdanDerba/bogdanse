package com.profit.bogdan.nio;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

/**
 * Created by profit on 22.07.17.
 */
public class NioTest {

    @Test
    public void copy() {
        Path pathSource = Paths.get("/home/profit/IdeaProjects/bogdan/", "paths.txt");
        File destFile = new File("/home/profit/IdeaProjects/bogdan/newTest1.txt");
        File sourceFile = new File("/home/profit/IdeaProjects/bogdan/newTest1.txt");
        try (FileOutputStream fos = new FileOutputStream(destFile);
             FileInputStream fis = new FileInputStream(sourceFile)) {
            long noOfBytes = Files.copy(pathSource, fos);
            System.out.println(noOfBytes);
            Path destPath1 = Paths.get("/home/profit/IdeaProjects/bogdan/", "newTest2.txt");

            noOfBytes = Files.copy(fis, destPath1, StandardCopyOption.REPLACE_EXISTING);

            System.out.println(noOfBytes);

            Path destPath2 = Paths.get("/home/profit/IdeaProjects/bogdan/", "newTest3.txt");
            Path target = Files.copy(pathSource, destPath2, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(target.getFileName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void directory() throws IOException {
        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(perms);
        Path path = Paths.get("/home/bogdan/IdeaProjects/bogdan", "Parent", "Child");

        Files.createDirectories(path, fileAttributes);

        Files.createTempDirectory(path, "Concretepage");


    }


    @Test
    public void file() throws IOException {
        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(perms);
        Path path = Paths.get("/home/bogdan/IdeaProjects/bogdan/Parent/Child", "Concretepage3655138810485057076", "file.txt");


        Files.createFile(path, fileAttributes);

    }



}
