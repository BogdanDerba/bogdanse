package com.profit.bogdan.interfaces;

import org.junit.Test;

import java.util.*;

/**
 * Created by profit on 18.06.17.
 */
public class FruitTest {
    @Test
    public void sortAnArrayTest() {
        String[] fruits = new String[]{"Pineapple", "Apple", "Orange", "Banana "};

        Arrays.sort(fruits);
        int i = 0;
        for (String temp : fruits
                ) {
            System.out.println("fruits number " + ++i + " : " + temp);

        }
    }

    @Test
    public void sortAnArrayList() {

        List<String> fruits = new
                ArrayList<>();
        fruits.add("Pineapple");
        fruits.add("Apple");
        fruits.add("Orange");
        fruits.add("Banana");

        Collections.sort(fruits);

        int i = 0;
        for (String temp : fruits
                ) {
            System.out.println("fruits number " + ++i + " : " + temp);

        }
    }


}
