package com.profit.bogdan.interfaces;

import org.junit.Test;

import java.util.Arrays;

/**
 * Created by profit on 18.06.17.
 */
public class sortFruitObjects {
    @Test
    public void sortFriutObject() {
        Fruit[] fruits = new Fruit[4];

        Fruit pinapple = new Fruit("Pinapple", "Pinapple description", 70);
        Fruit apple = new Fruit("Apple", "Apple description", 80);
        Fruit orange = new Fruit("Orange", "Orange description", 90);
        Fruit banana = new Fruit("Banana", "Banana description", 100);

        fruits[0] = pinapple;
        fruits[1] = apple;
        fruits[2] = orange;
        fruits[3] = banana;

        //  Arrays.sort(fruits);

        Arrays.sort(fruits, Fruit.fruitNameComparator);


        int i = 0;
        for (Fruit temp : fruits
                ) {
            System.out.println("fruits  " + ++i + " : " + temp.getFruitName() + " , Quantity : " + temp.getQuantity());

        }

    }
}
