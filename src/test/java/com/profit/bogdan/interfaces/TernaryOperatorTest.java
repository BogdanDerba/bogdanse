package com.profit.bogdan.interfaces;

import com.profit.bogdan.operators.TernaryOperator;
import org.junit.Test;

/**
 * Created by profit on 18.06.17.
 */
public class TernaryOperatorTest {
    @Test
    public void ternary() {
        String str = "Australia";

        int i = 10;
        TernaryOperator ternaryOperator = new TernaryOperator(5, 10);

        TernaryOperator.ternaryMethod(i, ternaryOperator);
        System.out.println(TernaryOperator.getMinValue(4, 10));
        System.out.println(TernaryOperator.getAbsoluteValue(-10));
        System.out.println(TernaryOperator.invertBoolean(false));

        TernaryOperator.containsA(str);
    }
}
