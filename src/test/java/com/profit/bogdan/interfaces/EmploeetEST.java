package com.profit.bogdan.interfaces;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
/**
 * Created by profit on 18.06.17.
 */
public class EmploeetEST {

    @Test
    public void testEmploeeSorting(){

        Employee eOne = new Employee(1, "A", 1000, 32, new Date(2011, 10, 1));
         Employee eTwo = new Employee( 2, "AB", 1300, 22, new Date(2012, 10, 1));
        Employee eThree = new Employee(3, "B", 10, 42, new Date(2010, 11, 3));
      Employee eFour = new Employee(4, "CD", 100, 23, new Date(2011, 10, 1));
        Employee eFive = new Employee(5, "AAA", 1200, 26, new Date(2011, 10, 1));

        List<Employee> listOfEmployees = new ArrayList<>();

        listOfEmployees.add(eOne);
        listOfEmployees.add(eTwo);
        listOfEmployees.add(eThree);
        listOfEmployees.add(eFour);
        listOfEmployees.add(eFive);
        System.out.println("Unsorted List : " + listOfEmployees);

       Collections.sort(listOfEmployees);
       assertEquals(eOne, listOfEmployees.get(0));
       assertEquals(eFive, listOfEmployees.get(4));

        System.out.println("Sorting by natural order : " + listOfEmployees );


        Collections.sort(listOfEmployees, Employee.nameComparator);
        assertEquals(eOne, listOfEmployees.get(0));
        assertEquals(eFour, listOfEmployees.get(4));


        System.out.println("Sorting by name order : " + listOfEmployees );


        Collections.sort(listOfEmployees, Employee.ageComparator);
        assertEquals(eTwo, listOfEmployees.get(0));
        assertEquals(eThree, listOfEmployees.get(4));

        System.out.println("Sorting by age order : " + listOfEmployees );


        Collections.sort(listOfEmployees, Employee.salaryComparator);
        assertEquals(eThree, listOfEmployees.get(0));
        assertEquals(eTwo, listOfEmployees.get(4));

        System.out.println("Sorting by salary order : " + listOfEmployees );

        Collections.sort(listOfEmployees, Employee.dateOfJoiningComparator);
        assertEquals(eThree, listOfEmployees.get(0));
        assertEquals(eTwo, listOfEmployees.get(4));

        System.out.println("Sorting by date order : " + listOfEmployees );








    }




}
