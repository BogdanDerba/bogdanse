package com.profit.bogdan.inerTest;

import com.profit.bogdan.inner.Cache;
import com.profit.bogdan.inner.LocalInner;
import com.profit.bogdan.inner.MyInterface;
import com.profit.bogdan.inner.Outer;
import org.junit.Test;

import java.util.Locale;

/**
 * Created by profit on 01.07.17.
 */
public class OutorTest {

    @Test
    public void outerTest() {
        Outer.Nested nested = new Outer.Nested();

        nested.printNestedText();

        Outer outer = new Outer();

        Outer.Inner inner = outer.new Inner();

        inner.printText();

        outer.doIt();

        Outer outer1 = new Outer(){
            @Override
            public void doIt(){
                System.out.println("Anonimu class doIt");
            }
        };

        outer1.doIt();
    }

    @Test
    public void myIntTest(){
        final String textToPrint = "Text...";
        MyInterface myInterface = new MyInterface() {

            private String text;

            {this.text = textToPrint;}
            @Override
            public void doIt() {

                System.out.println("Anonymus class doIt()");
                System.out.println(this.text);

            }
        };

        myInterface.doIt();
    }


    @Test
    public void cacheTest(){
        String key  = "Password";
        Integer id  = 1567888;

        Cache cache = new Cache();
        cache.store(key, id);

        System.out.println(cache.get("Password"));
        System.out.println(cache.getData("Password"));
    }

    @Test
    public void local(){
        LocalInner obj = new LocalInner();
        obj.display();
    }
}
