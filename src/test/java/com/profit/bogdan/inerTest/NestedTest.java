package com.profit.bogdan.inerTest;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.*;
import org.junit.runner.RunWith;

/**
 * Created by profit on 01.07.17.
 */
@RunWith(HierarchicalContextRunner.class)
public class NestedTest {

    @BeforeClass
    public static void beforeAllTestMethods() {
        System.out.println(" Invoked once before all");
    }

    @Before
    public void beforeEachTestMethod() {
        System.out.println(" Invoked beforer each");
    }

    @After
    public void afterEachTestMethod() {
        System.out.println("Invoked after each");
    }

    @AfterClass
    public static void afterAllTestMethod() {
        System.out.println("Invoked once after each");
    }

    public class ContextA {
        @Before

        public void beforeEachTestMethodContextA() {
            System.out.println("Invoked before each context A");
        }

        @After
        public void afterEachTestMethodContextA() {
            System.out.println("Invoked after  each context A");
        }

        @Test
        public void contextATest() {
            System.out.println("test of class A");
        }

        public class ContexC {

            @Before
            public void beforeEachTestMethodContextc() {
                System.out.println("Invoked before each context A");
            }

            @After
            public void afterEachTestMethodContextc() {
                System.out.println("Invoked after  each context A");
            }

            @Test
            public void contextCTest() {
                System.out.println("test of class C");
            }
        }
    }

}
