package com.profit.bogdan.jUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by profit on 25.06.17.
 */
@RunWith(Parameterized.class)
public class ParamTestFieldtwo {

private int mOne;
private int mTwo;


public ParamTestFieldtwo(int p1, int p2){
    mOne = p1;
    mTwo = p2;
}

@Parameterized.Parameters
    public static Collection<Object[]> data(){
    Object[][] data = new Object[][] {{1,2},{5,3},{121,4}};

    return Arrays.asList(data);
}
@Test
    public void testMultiplyEx(){
        Calculator tester = new Calculator();
        assertEquals("Result", mOne * mTwo, tester.multiply(mOne,mTwo));

    }
}
