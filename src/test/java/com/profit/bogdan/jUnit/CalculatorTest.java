package com.profit.bogdan.jUnit;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by profit on 25.06.17.
 */
public class CalculatorTest {

    @Test
    public void tastFindMax(){
        assertEquals(6,Calculator.findMax(new int[] {1,3,5,6,3}));
        assertEquals(5, Calculator.findMax(new int[]{ -12, -3, 4, 5, -31}));
        assertNotEquals(4, Calculator.findMax(new int[]{ -12, -3, 4, 5, -31}));

    }

    @Test
    public void multiplicationOfZeroIntegerShoudReturnZero(){
        Calculator calculator = new Calculator();

        assertEquals("10 * 0 must be 0", 0, calculator.multiply(10, 0));
        assertEquals("0 * 10 must be 0", 0, calculator.multiply(0, 10));
        assertEquals("0 * 0 must be 0", 0, calculator.multiply(0, 0));
    }
}
