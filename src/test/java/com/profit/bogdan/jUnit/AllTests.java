package com.profit.bogdan.jUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by profit on 25.06.17.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CalculatorTest.class,
        CalculatorTestTwo.class
})
public class AllTests {


}
