package com.profit.bogdan.jUnit;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
/**
 * Created by profit on 25.06.17.
 */
public class CalculatorTestTwo {
    @BeforeClass
    public static  void  setUpBeforeClass() throws Exception{
        System.out.println("before class");
    }
    @Before
    public void  setUp() throws Exception{
        System.out.println("before");
    }
    @Test
    public void terstFindMax(){
        System.out.println("test case find max");
        assertEquals(4, Calculator.findMax(new int[] {1,3,4,2}));
        assertEquals(-2, Calculator.findMax(new int[] {-12,-3,-4,-2}));
    }

    @Test
    public void testCube(){
        System.out.println("test case cube");
        assertEquals(27, Calculator.cube(3));
    }

    @Test
    public void testReverseWord() {
        System.out.println("test case reverse word");
        assertEquals("ym eman si nahk ", Calculator.reverseWord("my name is khan "));
        assertEquals("ym eman si nahk ", Calculator.reverseWord("my name is khan "));
    }
        @After
        public void tearDown() throws Exception{
        System.out.println("after");
        }

        @AfterClass
    public static void  tearDownAfterClass() throws Exception{
            System.out.println("after class");
        }
    }





