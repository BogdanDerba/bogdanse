package com.profit.bogdan.serialization;

import com.profit.bogdan.collections.Empl;
import com.profit.bogdan.serializatoin.Address;
import com.profit.bogdan.serializatoin.Person;
import com.profit.bogdan.serializatoin.User;
import com.profit.bogdan.taskForEmploee.Company;
import com.profit.bogdan.taskForEmploee.Emploee;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by profit on 23.07.17.
 */
public class SerializeTest {
    @Test
    public void ferst() {
        String fileName = "/home/profit/IdeaProjects/bogdan/time.txt";
        Person p = new Person("Lars", "Vogel");
//save  the obj to file;
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream out = new ObjectOutputStream(fos);
             FileInputStream fis = new FileInputStream(fileName);
             ObjectInputStream in = new ObjectInputStream(fis)) {
            out.writeObject(p);
            p = (Person) in.readObject();

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(p);
    }

    @Test
    public void second() {
        // ArrayList<Object> userArray= new  ArrayList();
        //Create new User object;
        //userArray.add(User myDetails = new User("Alex", "Smith", 1234567, new Date(Calendar.getInstance().getTimeInMillis()));
        //userArray.add(User myDetails = new User("Alex2", "Smith", 1234567, new Date(Calendar.getInstance().getTimeInMillis()));
        //userArray.add(User myDetails = new User("Alex3", "Smith", 1234567, new Date(Calendar.getInstance().getTimeInMillis()));
        //userArray.add(User myDetails = new User("Alex4", "Smith", 1234567, new Date(Calendar.getInstance().getTimeInMillis()));
        //userArray.add(User myDetails = new User("Alex5", "Smith", 1234567, new Date(Calendar.getInstance().getTimeInMillis()));
        //userArray.add(User myDetails = new User("Alex6", "Smith", 1234567, new Date(Calendar.getInstance().getTimeInMillis()));


        User myDetails = new User("Alex", "Smith", 1234567, new Date(Calendar.getInstance().getTimeInMillis()));

//Serialization code
        try (FileOutputStream fileOut = new FileOutputStream("User.txt");
             ObjectOutputStream out = new ObjectOutputStream(fileOut);
             FileInputStream fileIn = new FileInputStream("User.txt");
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            out.writeObject(myDetails);

            //DE - serialization code
            User deserializedUser = null;

            try {
                deserializedUser = (User) in.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


            // verify the object state
            System.out.println(deserializedUser.getFirstName());
            System.out.println(deserializedUser.getSecondName());
            System.out.println(deserializedUser.getAccountNumber());
            System.out.println(deserializedUser.getDateOpened());

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


    @Test
    public void thrid() {
        Address address = new Address();
        address.setStreet("wall street");
        address.setCountry("United state");

        SerializeTest.serializeAddress(address);

        Address address1 = SerializeTest.deserializeAddress("/home/profit/IdeaProjects/bogdan/");
        System.out.println(address1);


    }

    public static void serializeAddress(Address address) {
        try (FileOutputStream fout = new FileOutputStream("address.ser");
             ObjectOutputStream oos = new ObjectOutputStream(fout)) {
            oos.writeObject(address);
            System.out.println("Done");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static Address deserializeAddress(String fileName) {
        Address address = null;

        FileInputStream fin = null;
        ObjectInputStream ois = null;

        try {
            fin = new FileInputStream(fileName);
            ois = new ObjectInputStream(fin);
            address = (Address) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
        return address;

    }


    @Test
    public void companiColection() {

        ArrayList<Emploee> emploeeArray = new ArrayList();

        emploeeArray.add(new Emploee("Alex", "Smith", "seo", 123));
        emploeeArray.add(new Emploee("Al", "Smith", "ing", 123));
        emploeeArray.add(new Emploee("Sid", "Rik", "seo", 123));
        emploeeArray.add(new Emploee("Igor", "Smith", "seo", 123));
        emploeeArray.add(new Emploee("Max", "Smith", "seo", 123));
        emploeeArray.add(new Emploee("Alex", "Smith", "seo", 123));

        Company company = new Company();
        company.setaNewCompany(emploeeArray);

        try (FileOutputStream fileOut = new FileOutputStream("Emploee.txt");
             ObjectOutputStream out = new ObjectOutputStream(fileOut);
             FileInputStream fileIn = new FileInputStream("Emploee.txt");
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            out.writeObject(company);

            try {
                company = (Company)in.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            System.out.println(company);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    }

