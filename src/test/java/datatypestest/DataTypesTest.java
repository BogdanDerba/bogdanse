package datatypestest;

import org.junit.Test;
import com.profit.bogdan.datatypes.DataTypes;

import static com.profit.bogdan.firstapp.FirstApp.println;


/**
 * Created by profit on 28.05.17.
 */
public class DataTypesTest {

    @Test
    public void DataType() {

        DataTypes dataTypes = new DataTypes((byte) 5, (short) 10, 45, 167, true, 'f', 234, 1);
        println(dataTypes.getaByte());
        println(dataTypes.getaShort());
        println(dataTypes.getAnInt());
        println(dataTypes.getaLong());
        println(dataTypes.getaChar());
        println(dataTypes.getaDouble());
        println(dataTypes.getaFloat());
        println(dataTypes.getaBoolean());
    }

    @Test
    public void DataType2() {
        DataTypes dataTypes = new DataTypes((byte) 5, (short) 10, 45, 167, true, 'f', 234, 1);
        println(dataTypes.getaByte() + dataTypes.getaShort() + dataTypes.getAnInt()
                + dataTypes.getaLong() + dataTypes.getaChar()
                + dataTypes.getaDouble() + dataTypes.getaFloat());
    }

}
