package datatypestest;

import com.profit.bogdan.reports.SimpleReport;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by profit on 24.06.17.
 */
public class item {
    @Test
    public void createDataSource() {


        DRDataSource dataSource = new DRDataSource("item", "arderdate", "quantity", "unitprice");
        for (int i = 0; i < 20; i++) {
            dataSource.add("Book", new Date(), (int) (Math.random() * 10000) + 1, new BigDecimal(Math.random() * 100 + 1));

            new SimpleReport();
        }

    }
}
