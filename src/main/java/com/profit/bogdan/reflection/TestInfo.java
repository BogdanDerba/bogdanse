package com.profit.bogdan.reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by profit on 08.07.17.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TestInfo {
    /**
     *
     */
    enum Priority {
        /**
         *
         */
        LOW,
        /**
         *
         */
        MEDIUM,
        /**
         *
         */
        HIGH

    }

    /**
     * @return
     */
    Priority priority() default Priority.MEDIUM;

    /**
     * @return
     */
    String[] tags() default "";

    /**
     * @return
     */
    String createdBy() default "Bogdan";

    /**
     * @return
     */
    String lastModified() default "08/07/2017";
}
