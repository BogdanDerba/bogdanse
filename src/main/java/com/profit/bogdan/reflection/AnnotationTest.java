package com.profit.bogdan.reflection;

/**
 * Created by profit on 08.07.17.
 */
@TestInfo(
        priority = TestInfo.Priority.HIGH,
        createdBy = "Bogdan",
        tags = {"sales", "test"}
)
public class AnnotationTest {

    @Test
    void testA() {
        if (true) {
            throw new RuntimeException("This test always failed");
        }

    }

    @Test(enabled = false)
    void testB() {
        if (false) {
            throw new RuntimeException("This test always passed");
        }
    }

    @Test(enabled = true)
    void testC() {
        if (10 > 1) {

            return;
        }
    }
}
