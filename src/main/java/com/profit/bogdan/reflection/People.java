package com.profit.bogdan.reflection;

import java.io.Serializable;

/**
 * Created by profit on 08.07.17.
 */
public class People implements Serializable, Cloneable {
    /**
     * @return
     */
    private String name;
    /**
     * @return
     */
    private int age;
    /**
     * @return
     */
    private int sum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Deprecated
    protected static void method(String[] params) {
    }
}
