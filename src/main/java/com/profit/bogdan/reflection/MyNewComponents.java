package com.profit.bogdan.reflection;

import java.lang.annotation.*;

/**
 * Created by profit on 08.07.17.
 */
@Retention(RetentionPolicy.RUNTIME)
//@Target(ElementType.METHOD)
@Inherited
public @interface MyNewComponents {
    /**
     * @return
     */
    String value();

    /**
     * @return
     */
    String name();

    /**
     * @return
     */
    String age();

    /**
     * @return
     */
    String[] newNames();

    /**
     * @return
     */
    String element() default "elem";
}
