package com.profit.bogdan.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by profit on 08.07.17.
 */
public class RunTest {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        System.out.println("Testing...");

        int passed = 0;
        int failed = 0;
        int count = 0;
        int ignore = 0;

        Class<AnnotationTest> obj = AnnotationTest.class;

        //

        if (obj.isAnnotationPresent(TestInfo.class)) {

            Annotation annotation = obj.getAnnotation(TestInfo.class);
            TestInfo testerInfo = (TestInfo) annotation;

            System.out.printf("inPriority :%s", testerInfo.priority());
            System.out.printf("inCreatedBy :%s", testerInfo.createdBy());
            System.out.printf("%nTags: ");

            int tagLength = testerInfo.tags().length;
            for (String tag : testerInfo.tags()
                    ) {
                if (tagLength > 1) {
                    System.out.print(tag + ", ");
                } else {
                    System.out.print(tag);
                }
                tagLength--;

            }
            System.out.printf("%nLastModified : %s%n%n", testerInfo.lastModified());
        }

        for (Method method : obj.getDeclaredMethods()
                ) {
            if (method.isAnnotationPresent(Test.class)) {
                Annotation annotation = method.getAnnotation(Test.class);

                Test test = (Test) annotation;

                if (test.enabled()) {
                    try {
                        method.invoke(obj.newInstance());
                        System.out.printf("%s - Test '%s' - passed %n ", ++count, method.getName());
                        passed++;
                    } catch (Throwable ex) {
                        System.out.printf("%s - Test '%s' - failed: %s %n ", ++count, method.getName(), ex.getCause());
                        failed++;
                    }
                } else {
                    System.out.printf("%s - Test  '%s'  - ignored %n", ++count, method.getName());
                    ignore++;
                }

            }

        }
        System.out.printf("%nResult : Total : %d, Passed : %d, Failed %d, Ignore %d%n", count, passed, failed, ignore);
    }
}
