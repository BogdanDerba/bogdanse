package com.profit.bogdan.serializatoin;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by profit on 23.07.17.
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1234567890L;

    private String firstName;
    private String secondName;
    private int accountNumber;
    private Date dateOpened;

    public User(String firstName, String secondName, int accountNumber, Date dateOpened) {
        super();
        this.firstName = firstName;
        this.secondName = secondName;
        this.accountNumber = accountNumber;
        this.dateOpened = dateOpened;
    }
    public User(){
        super();
    }



    public final String getFirstName() {
        return firstName;
    }

    public final void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public final String getSecondName() {
        return secondName;
    }

    public final void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public final int getAccountNumber() {
        return accountNumber;
    }

    public final void setAccountNumber(int aNewAccountNumber) {
        this.accountNumber = aNewAccountNumber;
    }

    public Date getDateOpened() {
        return new Date (dateOpened.getTime());
    }

    public final void setDateOpened(Date dateOpened) {
       Date newDate = new Date(dateOpened.getTime());
       dateOpened = newDate;
    }

    private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException{
        firstName = aInputStream.readUTF();
        secondName = aInputStream.readUTF();
        accountNumber = aInputStream.readInt();
        dateOpened = new Date(aInputStream.readLong());
    }
    private void writeObject(ObjectOutputStream aOutputStream) throws IOException{
        aOutputStream.writeUTF(firstName);
        aOutputStream.writeUTF(secondName);
        aOutputStream.writeInt(accountNumber);
        aOutputStream.writeLong(dateOpened.getTime());
    }
}
