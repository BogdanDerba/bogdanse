package com.profit.bogdan.serializatoin;

import java.io.Serializable;

/**
 * Created by profit on 23.07.17.
 */
public class Address implements Serializable {

    private  static  final long serialVersionUID = 1L;

    String street;
    String country;

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return new StringBuffer(" Street : ").append(this.street)
                .append("Country : ")
                .append(this.country).toString();
    }
}
