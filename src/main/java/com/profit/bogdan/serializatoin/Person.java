package com.profit.bogdan.serializatoin;

import java.io.Serializable;

/**
 * Created by profit on 23.07.17.
 */
public class Person implements Serializable {

    private String firstName;
    private String secondName;

    //
    transient private Thread myThread;

    public Person(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.myThread = new Thread();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public String toString() {
        return "Person [firstName = " + firstName + ", secondName = "+ secondName+ "]";
    }
}
