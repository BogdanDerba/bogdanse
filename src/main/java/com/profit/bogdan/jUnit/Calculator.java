package com.profit.bogdan.jUnit;

import java.util.StringTokenizer;

/**
 * Created by profit on 25.06.17.
 */
public class Calculator {
    /**
     * @param x
     * @param y
     * @return
     */
    public static int sum(int x, int y) {
        return x + y;
    }

    /**
     * @param x
     * @param y
     * @return
     */
    public int multiply(int x, int y) {
        return x * y;
    }

    /**
     * @param arr
     * @return
     */
    public static int findMax(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        return max;
    }

    /**
     * @param n
     * @return
     */
    public static int cube(int n) {
        return n * n * n;
    }

    /**
     * @param str
     * @return
     */
    public static String reverseWord(String str) {

        StringBuilder result = new StringBuilder();
        StringTokenizer tokenizer = new StringTokenizer(str, " ");

        while (tokenizer.hasMoreTokens()) {
            StringBuilder sb = new StringBuilder();
            sb.append(tokenizer.nextToken());
            sb.reverse();

            result.append(sb);
            result.append(" ");
        }
        return result.toString();
    }
}
