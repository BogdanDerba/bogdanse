package com.profit.bogdan.conditional;


/**
 * Created by profit on 28.05.17.
 */
public class ConditionalStatements {

    protected ConditionalStatements() {

    }

    /**
     * Check if value is positive or negative
     * @param x
     */
    public static void positiveOrNegative(Integer x) {
        int input = x;
        if (input > 0) {
            System.out.println("Number is positive");
        } else if (input < 0) {
            System.out.println("Number is negativ");
        } else {
            System.out.println("Number is zero");
        }

    }
}
