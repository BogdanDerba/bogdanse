package com.profit.bogdan.inner;

/**
 * Created by profit on 01.07.17.
 */
public class LocalInner {
    /**
     *
     */
    private String message = "Bogdan.com";

    /**
     *
     */
    public void display() {
        final int data = 20;
        /**
         *
         */
        class Local {
            void msg() {
                System.out.println(message + "  :  " + data);
            }
        }

        Local loc = new Local();

        loc.msg();
    }
}
