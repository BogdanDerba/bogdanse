package com.profit.bogdan.datatypes;

/**
 * Created by profit on 28.05.17.
 */
public class DataTypes {
    /**
     *
     */
    private byte aByte;

    /**
     *
     * @return byte
     */
    public byte getaByte() {
        return aByte;
    }

    /**
     *
     */
    private short aShort;

    /**
     *
     * @return short
     */
    public short getaShort() {
        return aShort;
    }

    /**
     *
     */
    private int anInt;

    /**
     *
     * @return int
     */
    public int getAnInt() {
        return anInt;
    }

    /**
     *
     */
    private long aLong;

    /**
     *
     * @return long
     */
    public long getaLong() {
        return aLong;
    }

    /**
     *
     */
    private boolean aBoolean;

    /**
     *
     * @return boolean
     */
    public boolean getaBoolean() {
        return aBoolean;
    }

    /**
     *
     */
    private char aChar;

    /**
     *
     * @return char
     */
    public char getaChar() {
        return aChar;
    }

    /**
     *
     */
    private float aFloat;

    /**
     *
     * @return float
     */
    public float getaFloat() {
        return aFloat;
    }

    /**
     *
     */
    private double aDouble;

    /**
     *
     * @return double
     */
    public double getaDouble() {
        return aDouble;
    }

    public DataTypes(byte aByte, short aShort, int anInt, long aLong,
                     boolean aBoolean, char aChar, float aFloat, double aDouble) {
        this.aByte = aByte;
        this.aShort = aShort;
        this.anInt = anInt;
        this.aLong = aLong;
        this.aBoolean = aBoolean;
        this.aChar = aChar;
        this.aFloat = aFloat;
        this.aDouble = aDouble;
    }
}
