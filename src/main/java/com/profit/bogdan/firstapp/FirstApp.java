package com.profit.bogdan.firstapp;

import static java.lang.System.*;

/**
 * Created by profit on 27.05.17.
 */
public class FirstApp {
    /**
     * print method
     *
     * @param t
     * @param <T>
     */

    public static <T> void print(T t) {
        out.print(t);
    }

    /**
     * print method that prints Objects
     *
     * @param object
     */

    public static void println(Object object) {
        out.println(object);
    }

    /**
     * print method that new line
     */

    public static void println() {
        out.println();
    }
}
