package com.profit.bogdan.strings;

import java.util.StringTokenizer;

/**
 * Created by profit on 10.06.17.
 */
public class StringTokenazing {
    /**
     *
     * @param str
     */
    public static void splitBySpace(String str) {

        StringTokenizer st = new StringTokenizer(str);

        System.out.println("..... Split by space ......");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     *
     * @param str
     */
    public static void splitByComma(String str) {

        System.out.println(".... Split by comma ','....");
        StringTokenizer st = new StringTokenizer(str, ",");

        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     *
     * @param str
     */
    public static void splitByDefis(String str) {

        System.out.println(".... Split by defis ','....");
        StringTokenizer st = new StringTokenizer(str, "-");

        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }
}
