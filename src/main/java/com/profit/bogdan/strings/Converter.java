package com.profit.bogdan.strings;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by profit on 10.06.17.
 */
public class Converter {
    /**
     * @param xmlSource
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public static Document stringToDom(String xmlSource)
            throws SAXException, ParserConfigurationException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.parse(new InputSource(new StringReader(xmlSource)));

    }

    /**
     * document to string
     *
     * @param doc
     * @return
     * @throws TransformerException
     */
    public String documentToString(org.w3c.dom.Document doc) throws TransformerException {

        //
        DOMSource domSource = new DOMSource(doc);

        //
        StringWriter stringWriter = new StringWriter();

        //
        StreamResult result = new StreamResult(stringWriter);

        //
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        transformer.setOutputProperty("indent", "");

        //
        transformer.transform(domSource, result);
        return stringWriter.toString();

    }

    /**
     * @param filename
     * @return
     * @throws IOException
     */
    public static StringBuilder lineXml(String filename) throws IOException {

        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            String line;


            while ((line = br.readLine()) != null) {
                sb.append(line.trim());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb;
    }

    /**
     *
     * @param xmlSource
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerException
     */
    public static void stringToDomFile(String xmlSource)
            throws SAXException, ParserConfigurationException, IOException, TransformerException {


        //Parse the given input
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xmlSource)));

        //Write the parsed document to an xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StreamResult result = new StreamResult(new File("my-file.xml"));
        transformer.transform(source, result);
    }
}
