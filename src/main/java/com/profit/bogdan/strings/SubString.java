package com.profit.bogdan.strings;

/**
 * Created by profit on 10.06.17.
 */
public class SubString {
    /**
     *
     * @param str
     * @return
     */
    public static String subStringSearch(String str) {

        StringBuilder result = new StringBuilder();
        int length = str.length();

        for (int i = 0; i < length / 2; i++) {
            char symbol = str.charAt(i);
            if (symbol == str.charAt(length - (i + 1))) {
                result.append(symbol);
            } else {
                break;
            }
        }
        return result.toString();
    }
}
