package com.profit.bogdan.loopsandarrays;

import static com.profit.bogdan.firstapp.FirstApp.*;

import java.util.Arrays;


/**
 * Created by profit on 03.06.17.
 */
public class ForLoops {
    /**
     * @param myArrayOne
     * @param myArrayTwo
     */
    public static void equalityOfTwoArrays(int[] myArrayOne, int[] myArrayTwo) {

        boolean equalOrNot = true;

        if (myArrayOne.length == myArrayTwo.length) {
            for (int i = 0; i < myArrayOne.length; i++) {
                if (myArrayOne[i] != myArrayTwo[i]) {
                    equalOrNot = false;

                }
            }
        } else {
            equalOrNot = false;
        }
        if (equalOrNot) {
            println("Two arrays are equal.");
        } else {
            println("two arrays are not equal!");
        }
    }

    /**
     *
     * @param myArray
     */
    public static void uniqueArray(int[] myArray) {

        int arraySize = myArray.length;

        println("Original Array : ");

        for (int i = 0; i < arraySize; i++) {
            print(myArray[i] + "\t");

        }

        for (int i = 0; i < arraySize; i++) {

            for (int j = i + 1; j < arraySize; j++) {

                if (myArray[i] == myArray[j]) {

                    myArray[j] = myArray[arraySize - 1];
                    arraySize--;
                    j--;
                }
            }

        }

        int[] arrayO = Arrays.copyOf(myArray, arraySize);

        println();
        println("Array with unique values : ");

        for (int i = 0; i < arrayO.length; i++) {
            print(arrayO[i] + "\t");
        }
            println();


    }


}



