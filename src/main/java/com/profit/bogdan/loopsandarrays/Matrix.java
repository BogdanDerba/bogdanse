package com.profit.bogdan.loopsandarrays;

/**
 * Created by profit on 03.06.17.
 */
public class Matrix {
    /**
     *
     * @param first
     * @param second
     * @return
     */
    public static Double[][] multiplicar(Double[][] first, Double[][] second) {

        int firstRows = first.length;
        int firstColumns = first[0].length;
        int secondRows = second.length;
        int secondColumns = second[0].length;

        if (firstColumns != secondRows) {
            throw new IllegalArgumentException("First Rows: " + firstColumns
                    + " did not match Sedcond columns " + secondRows + ".");
        }

        Double[][] result = new Double[firstRows][secondColumns];
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                result[i][j] = 0.00000;
            }


        }

        for (int i = 0; i < firstRows; i++) {
            for (int j = 0; j < secondColumns; j++) {
                for (int k = 0; k < result.length; k++) {
                    result[i][j] += first[i][k] * second[k][j];
                }

            }

        }


        return result;
    }

}
