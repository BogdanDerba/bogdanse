package com.profit.bogdan.loopsandarrays;

/**
 * Created by profit on 10.06.17.
 */
public class Binari {

    /**
     *
     * @param array
     * @param target
     */
    public static void binarySearch(int[] array, int target) {

        int low = 0;
        int high = array.length - 1;

        while (high >= low) {
            int middle = (low + high) / 2;
            if (array[middle] == target) {
                System.out.println(array[middle]);
                break;
            }
            if (array[middle] < target) {
                low = middle + 1;
            }
            if (array[middle] > target) {
                high = middle - 1;
            }
            if (array[middle] != target) {
                System.out.println("UnFound");
            }
        }


    }
}
