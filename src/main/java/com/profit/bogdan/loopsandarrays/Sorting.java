package com.profit.bogdan.loopsandarrays;


/**
 * Created by profit on 04.06.17.
 */
public class Sorting {
    /**
     *
     * @param num
     */
    public static void selectionSort(int[] num) {
        int first;
        int temp;

        for (int i = num.length - 1; i > 0; i--) {
            //initialize to subscript of first element
            first = 0;

            //locate smallest between position1 1 and i

            for (int j = 1; j <= i; j++) {
                if (num[j] < num[first]) {
                    first = j;
                }
            }

            //swap smallest foundwith element in position i


            temp = num[first];
            num[first] = num[i];
            num[i] = temp;
        }

    }

    /**
     *
     * @param array
     */
    public static void bubbleSort(int[] array) {
        boolean swapped = true;
        int j = 0;
        int tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < array.length - j; i++) {
                if (array[i] > array[i + 1]) {
                    tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
    }
}

