package com.profit.bogdan.loopsandarrays;

import static com.profit.bogdan.firstapp.FirstApp.*;
/**
 * Created by profit on 28.05.17.
 */
public class WhileLoops {
    /**
     * @param str
     */
    public static void selectChoice(String str) {
        String choice;
        String con = "y";


        println("What is the command keyword to exit a loop in Java?");

        println("a.quit");
        println("b.continue");
        println("c.break");
        println("d.exit");

        while (con.compareTo("y") == 0) {

            println("Enter your choice:");

            choice = str;

            if (choice.compareTo("c") == 0) {
                println("Congratulation!");
            } else if (choice.compareTo("q") == 0
                    || choice.compareTo("e") == 0) {
                println("Exiting...!");
                break;
            } else {
                println("Incorrect!");
                break;
            }

        }
    }
}
