package com.profit.bogdan.functional;

/**
 * Created by aleksey on 28.07.17.
 */
@FunctionalInterface
public interface SimpleFuncInterface {
    void doWork();
    String toString();
    boolean equals(Object o);
}
