package com.profit.bogdan.functional;

/**
 * Created by aleksey on 28.07.17.
 */
@FunctionalInterface
public interface ComplexFunctionalInterface extends SimpleFuncInterface {

    static void doSomeWork(){
        System.out.println("Doing some work in interface impl...");
    }

    default void doSomeOtherWork(){
        System.out.println("Doing some other work in interface impl...");
    }
}
