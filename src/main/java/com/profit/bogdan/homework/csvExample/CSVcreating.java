package com.profit.bogdan.homework.csvExample;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

/**
 * Created by bogdan on 06.08.17.
 */
public class CSVcreating {
 @Test
    public void directory() {
        try {
            File folder = new File("/home/bogdan/IdeaProjects/bogdan/newDirForHomeWork");

            if (!folder.exists()) {
                folder.mkdir();
                try {
                    Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
                    FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(perms);
                    Path path = Paths.get("/home/bogdan/IdeaProjects/bogdan", "newDirForHomeWork", "newTable.csv");


                    Files.createFile(path, fileAttributes);

                } catch (IOException x) {
                    // Some other sort of failure, such as permissions.
                    System.err.format("createFile error: %s%n", x);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
