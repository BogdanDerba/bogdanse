package com.profit.bogdan.homework.csvExample;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by bogdan on 06.08.17.
 */
public class csvContent {
    @Test
    public void fileWriter() throws IOException {


        String csvFile = "/home/bogdan/IdeaProjects/bogdan/newDirForHomeWork/newTable.csv";
        FileWriter writer = new FileWriter(csvFile);

        List<csvObj> users = Arrays.asList(
                new csvObj("Sergey Brin", 12500, 32),
                new csvObj("Antony Acenberg", 10099, 25),
                new csvObj("John Weak", 9990, 31),
                new csvObj("Serg Lavrov", 12000, 32),
                new csvObj("Alex Shumskiy", 18799, 51),
                new csvObj("John Dilinger", 10999, 30),
                new csvObj("Sir Clown", 12500, 33),
                new csvObj("Anton Elchin", 15099, 25),
                new csvObj("Johnny Mnemonik", 19999, 21),
                new csvObj("Serg Gensburg", 20500, 24),
                new csvObj("Artem Sidorov", 21099, 25),
                new csvObj("Jose Cuervo", 98000, 34)
        );

       
        CSVUtils.writeLine(writer, Arrays.asList("Name", "Salary", "Age"));

        for (csvObj d : users) {

            List<String> list = new ArrayList<>();
            list.add(d.getName());
            list.add(String.valueOf(d.getSalary()));
            list.add(String.valueOf(d.getAge()));


            CSVUtils.writeLine(writer, list, '|', '\"');
        }

        writer.flush();
        writer.close();


    }


    @Test
    public void readStreamFile() {

        try {
            Stream<String> streamFromFiles = Files.lines(Paths.get("/home/bogdan/IdeaProjects/bogdan/newDirForHomeWork/newTable.csv"));


            List listOfStream = streamFromFiles.collect(Collectors.toList());


            String csvFile = "/home/bogdan/IdeaProjects/bogdan/newDirForHomeWork/newTableTwo.txt";
            FileWriter writer = new FileWriter(csvFile);


            List<String> usersTwo = listOfStream;
            CSVUtils.writeLine(writer, usersTwo, ' ', ' ');

            writer.flush();
            writer.close();



        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
