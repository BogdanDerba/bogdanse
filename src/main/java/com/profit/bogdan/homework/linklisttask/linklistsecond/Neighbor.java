package com.profit.bogdan.homework.linklisttask.linklistsecond;

/**
 * Created by bogdan on 10.07.17.
 */
public class Neighbor {
    /**
     *
     */
    public String homeOwenerName;
    /**
     *
     */
    public int houseNumber;
    /**
     *
     */
    public Neighbor next;
    /**
     *
     */
    public Neighbor previous;

    public Neighbor(String homeOwenerName, int houseNumber) {
        this.homeOwenerName = homeOwenerName;
        this.houseNumber = houseNumber;
    }

    /**
     *
     */
    public void display() {

        System.out.println(homeOwenerName + ": " + houseNumber + " Private Drive");

    }

    /**
     * @return
     */
    public String toString() {
        return homeOwenerName;
    }
}
