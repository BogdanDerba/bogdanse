package com.profit.bogdan.homework.linklisttask.linklistfirst;

/**
 * Created by bogdan on 10.07.17.
 */
public class Link {
    /**
     *
     */
    public String bookName;
    /**
     *
     */
    private int millionsSold;
    /**
     *
     */
    public Link next;

    protected Link(String bookName, int millionsSold) {

        this.bookName = bookName;
        this.millionsSold = millionsSold;
    }

    /**
     *
     */
    public void display() {
        System.out.println(bookName + " : " + millionsSold + ",000,000 copy sold");
    }

    /**
     * @return
     */
    public String toString() {
        return bookName;
    }


}
