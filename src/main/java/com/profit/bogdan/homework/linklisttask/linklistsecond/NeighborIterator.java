package com.profit.bogdan.homework.linklisttask.linklistsecond;

/**
 * Created by bogdan on 10.07.17.
 */
public class NeighborIterator {
    /**
     *
     */
    public Neighbor currentNeighbor;
    /**
     *
     */
    public Neighbor previousNeighbor;
    /**
     *
     */
    DoubleEndedLinkedList theNeighbors;

    public NeighborIterator(DoubleEndedLinkedList theNeighbors) {

        this.theNeighbors = theNeighbors;
        this.currentNeighbor = theNeighbors.firstLink;
        this.previousNeighbor = theNeighbors.lastLink;
    }

    /**
     * @return
     */
    public boolean hasNext() {

        if (currentNeighbor.next != null) {

            return true;

        }
        return false;
    }

    /**
     * @return
     */
    public Neighbor next() {

        if (hasNext()) {
            previousNeighbor = currentNeighbor;
            currentNeighbor = currentNeighbor.next;

            return currentNeighbor;
        }
        return null;
    }

    /**
     *
     */
    public void remove() {

        if (previousNeighbor == null) {

            theNeighbors.firstLink = currentNeighbor.next;
        } else {

            previousNeighbor.next = currentNeighbor.next;

            if (currentNeighbor.next == null) {

                currentNeighbor = theNeighbors.firstLink;
                previousNeighbor = null;

            } else {
                currentNeighbor = currentNeighbor.next;
            }
        }


    }
}
