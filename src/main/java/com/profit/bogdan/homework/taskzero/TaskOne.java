package com.profit.bogdan.homework.taskzero;

import static com.profit.bogdan.firstapp.FirstApp.*;
import static java.lang.Float.sum;


/**
 * Created by bogdan on 01.06.17.
 */
public class TaskOne {
    /**
     *
     * @param a
     * @param b
     */
    public static void evenAndOddOperation(int a, int b) {

        if ((a % 2) == 0) {
            println("a * b = " + (a * b));
        } else {
            println("a + b = " + sum(a, b));
        }

    }
}