package com.profit.bogdan.homework.taskzero;

import static com.profit.bogdan.firstapp.FirstApp.*;

/**
 * Created by bogdan on 02.06.17.
 */
public class TaskTwo {
    /**
     * @param a
     * @param b
     */
    public static void simpleSortForTwoDigits(int a, int b) {

        if (a > b) {
            println(b + " " + a);
        } else {
            println(a + " " + b);
        }

    }

    /**
     * @param a
     * @param b
     * @param c
     */
    public static void simpleSortForTreeDigits(int a, int b, int c) {

        if (a < b && b < c) {
            println(a + " " + b + " " + c);
        } else if (b < a && a < c) {
            println(b + " " + a + " " + c);
        } else if (a > c && a > b && b < c) {
            println(b + " " + c + " " + a);
        } else {
            println(c + " " + b + " " + a);
        }
    }

    /**
     * @param a
     * @param b
     * @param c
     * @param d
     */
    public static void simpleSortForFourDigit(int a, int b, int c, int d) {

        if (a < b && b < c && c < d) {
            println(a + " " + b + " " + c + " " + d);

        } else if (b < a && a < c && c < d) {
            println(b + " " + a + " " + c + " " + d);

        } else if (b < c && c < a && a < d) {
            println(b + " " + c + " " + a + " " + d);

        } else if (b < c && c < d && d < a) {
            println(b + " " + c + " " + d + " " + a);

        } else if (a < c && c < b && b < d) {
            println(a + " " + c + " " + b + " " + d);

        } else if (a < c && c < d && d < b) {
            println(a + " " + c + " " + d + " " + b);

        } else if (c < a && a < b && b < d) {
            println(c + " " + a + " " + b + " " + d);

        } else if (c < a && a < d && d < b) {
            println(c + " " + a + " " + d + " " + b);

        } else if (a < d && d < b && b < c) {
            println(a + " " + d + " " + b + " " + c);

        } else if (d < a && a < b && b < c) {
            println(d + " " + a + " " + b + " " + c);

        } else if (a < b && b < d && d < c) {
            println(a + " " + b + " " + d + " " + c);

        } else if (d < a && a < c && c < b) {
            println(d + " " + a + " " + c + " " + b);

        } else if (d < c && c < a && a < b) {
            println(d + " " + c + " " + a + " " + b);

        } else if (c < b && b < a && a < d) {
            println(c + " " + b + " " + a + " " + d);

        } else if (c < d && d < a && a < b) {
            println(c + " " + d + " " + a + " " + b);

        } else if (a < d && d < c && c < b) {
            println(a + " " + d + " " + c + " " + b);

        } else if (b < a && a < d && d < c) {
            println(b + " " + a + " " + d + " " + c);

        } else if (b < d && d < a && a < c) {
            println(b + " " + d + " " + a + " " + c);

        } else if (c < d && d < b && b < a) {
            println(c + " " + d + " " + b + " " + a);

        } else if (d < b && b < c && c < a) {
            println(d + " " + b + " " + c + " " + a);

        } else if (d < b && b < a && a < c) {
            println(d + " " + b + " " + a + " " + c);

        } else if (c < b && b < d && d < a) {
            println(c + " " + b + " " + d + " " + a);

        } else if (b < d && d < c && c < a) {
            println(b + " " + d + " " + c + " " + a);

        } else if (d < c && c < b && b < a) {
            println(d + " " + c + " " + b + " " + a);

        } else {
            println("no matches");
        }
    }
}
