package com.profit.bogdan.homework.taskzero;

import static com.profit.bogdan.firstapp.FirstApp.*;

/**
 * Created by bogdan on 01.06.17.
 */
public class TaskZero {
    /**
     *
     * @param x
     * @param y
     * @param z
     */
    public static void maxDigitOfThree(Integer x, Integer y, Integer z) {

        if (x > y && x > z) {
            println("Max digit is " + x);
        } else if (y > x && y > z) {
            println("Max digit is " + y);
        } else if (z > x && z > y) {
            println("Max digit is " + z);
        }

    }
}
