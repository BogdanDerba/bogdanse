package com.profit.bogdan.homework.taskzero;

import static com.profit.bogdan.firstapp.FirstApp.println;

/**
 * Created by bogdan on 01.06.17.
 */
public class TaskZeroTwo {
    /**
     *
     * @param x
     * @param y
     * @param z
     * @param q
     */
    public static void maxDigitOfFour(Integer x, Integer y, Integer z, Integer q) {

        if (x > y && x > z && x > q) {
            println("Max digit is " + x);
        } else if (y > x && y > z && y > q) {
            println("Max digit is " + y);
        } else if (z > x && z > y && z > q) {
            println("Max digit is " + z);
        } else {
            println("Max digit is " + q);
        }

    }
}
