package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by bogdan on 14.06.17.
 */
public class Parallelogram extends Trap {

    /**
     * Integer for double side;
     */
    public static final int DOUBLESIDE = 2;
    /**
     *  sin;
     */
    private double sin;


    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        this.sin = Math.sin(getAnglAB());
        return this.getSideA() * this.getSideB() * sin;

    }

    public double getPerimetr() {
        return calculatePerimetr();
    }

    private double calculatePerimetr() {
        return DOUBLESIDE * (this.getSideA() + this.getSideB());
    }


    public Parallelogram(double anglAB, double sideA, double sideB) {
        super(anglAB, sideA, sideB);

    }

}