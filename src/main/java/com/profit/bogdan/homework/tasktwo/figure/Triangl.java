package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by profit on 11.06.17.
 */
public class Triangl extends Figure {
    /**
     * side A field;
     */
    private double sideA;
    /**
     * side B field;
     */
    private double sideB;
    /**
     * side C field;
     */
    private double sideC;

    public Triangl(double area, double sideA, double sideB, double sideC) {
        super(area);
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
