package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by bogdan on 15.06.17.
 */
public class Circle extends Figure {
    public Circle(double radius) {
        super(radius);
    }

    /**
     * radius field;
     */
    private double radius = 1.0;

    public Circle(double radius, double area) {
        super(area);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return this.radius * this.radius * Math.PI;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
