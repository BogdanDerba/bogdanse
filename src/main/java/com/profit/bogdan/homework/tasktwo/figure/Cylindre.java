package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by bogdan on 15.06.17.
 */
public class Cylindre extends Circle {
    /**
     * height;
     */
    private double height;

    public Cylindre(double radius, double height) {
        super(radius);
        this.height = height;
    }

    /**
     * radius;
     */
    private double radius = 1.0;

    public Cylindre(double radius, double area, double height) {
        super(area);
        this.radius = radius;
        this.height = height;
    }

    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return (this.radius * this.radius * Math.PI) * this.height;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
