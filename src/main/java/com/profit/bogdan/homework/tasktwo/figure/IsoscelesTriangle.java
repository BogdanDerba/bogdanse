package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by bogdan on 15.06.17.
 */
public class IsoscelesTriangle extends Triangl {
    /**
     * Integer for fist digit;
     */
    public static final int DIGIT1 = 4;
    /**
     * Integer for second digit;
     */
    public static final int DIGIT2 = 2;

    public IsoscelesTriangle(double area, double sideA, double sideB, double sideC) {
        super(area, sideA, sideB, sideC);
    }

    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return (this.getSideB() * Math.sqrt(Math.pow(this.getSideA(), DIGIT2)
                - (Math.pow(this.getSideB(), DIGIT2) / DIGIT1))) / DIGIT2;
    }

    public double getPerimetr() {
        return calculatePerimetr();
    }

    private double calculatePerimetr() {
        return this.getSideA() + this.getSideB() + this.getSideC();
    }
}
