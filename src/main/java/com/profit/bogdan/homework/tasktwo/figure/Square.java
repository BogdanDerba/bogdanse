package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by profit on 11.06.17.
 */
public class Square extends Trap {
    /**
     * double side field;
     */
    private int doubleSide = 2;

    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return this.getSideA() * this.getSideB();
    }


    public double getPerimetr() {
        return calculatePerimetr();
    }

    private double calculatePerimetr() {
        return (this.getSideA() + this.getSideB()) * this.doubleSide;
    }

    public Square(double area, double sideA, double sideB) {
        super(area, sideA, sideB);
    }
}
