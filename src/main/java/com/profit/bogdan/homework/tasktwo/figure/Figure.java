package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by profit on 11.06.17.
 */
public class Figure {
    /**
     * area field;
     */
    private double area;
    /**
     * perimetr field;
     */
    private double perimetr;

    public double getPerimetr() {
        return perimetr;
    }

    public void setPerimetr(double perimetr) {
        this.perimetr = perimetr;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public Figure(double area) {

        this.area = area;

    }
}
