package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by bogdan on 15.06.17.
 */
public class RightTriangle extends Triangl {
    /**
     * half of one for easy calculate;
     */
    public static final double HALF = 0.5;

    public RightTriangle(double area, double sideA, double sideB, double sideC) {
        super(area, sideA, sideB, sideC);
    }

    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return this.HALF * this.getSideA() * this.getSideB();
    }

    public double getPerimetr() {
        return calculatePerimetr();
    }

    private double calculatePerimetr() {
        return this.getSideA() + this.getSideB() + this.getSideC();
    }
}
