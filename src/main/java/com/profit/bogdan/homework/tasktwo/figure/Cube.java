package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by bogdan on 15.06.17.
 */
public class Cube extends Square {
    /**
     * side number;
     */
    public static final int SIDENUMB = 3;

    public Cube(double area, double sideA, double sideB) {
        super(area, sideA, sideB);
    }

    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return this.getSideA() * this.SIDENUMB;
    }
}
