package com.profit.bogdan.homework.tasktwo.figure;

/**
 * Created by profit on 11.06.17.
 */
public class Trap extends Figure {
    /**
     * A side;
     */
    private double sideA;
    /**
     * B side;
     */
    private double sideB;
    /**
     * angle of A & B sides;
     */
    private double anglAB;

    public double getAnglAB() {
        return anglAB;
    }

    public void setAnglAB(double anglAB) {
        this.anglAB = anglAB;
    }

    public Trap(double area, double sideA, double sideB) {
        super(area);
        this.sideA = sideA;
        this.sideB = sideB;
    }


    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public double getArea() {
        return super.getArea();
    }

    @Override
    public void setArea(double area) {
        super.setArea(area);
    }

    public Trap(double area) {
        super(area);
    }
}
