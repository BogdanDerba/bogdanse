package com.profit.bogdan.homework.mapAndGenerics;

import org.junit.Test;

import java.util.*;

/**
 * Created by bogdan on 10.08.17.
 */
public class MapValueTwo {
    @Test
    public  void content1(){
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        map.put(20,30);
        map.put(21,55);
        map.put(17,52);
        map.put(23,22);

        System.out.println(map.toString());

        SortByValue.sortByValue(map);

        System.out.println(map.toString());
    }


    @Test
    public  void content3(){
       Map< String,Integer> map = new HashMap<String,Integer>();
        map.put("13",33);
        map.put("55", 44);
        map.put("52",55);
        map.put("22",23);

        System.out.println(map.toString());

        SortByValue.sortByValue(map);

        System.out.println(map.toString());
    }

}
