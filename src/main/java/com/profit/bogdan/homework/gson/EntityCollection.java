package com.profit.bogdan.homework.gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bogdan on 01.08.17.
 */
public class EntityCollection {
    public List<Entity> EntityList = new ArrayList<>();



    public List<Entity> getEntityList() {
        return EntityList;
    }

    public void setEntityList(List<Entity> entityList) {
        EntityList = entityList;
    }

    @Override
    public String toString() {
        return "EntityCollection{" +
                "EntityList=" + EntityList +
                '}';
    }
}
