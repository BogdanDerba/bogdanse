package com.profit.bogdan.taskForEmploee;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by profit on 23.07.17.
 */
public class Company implements Serializable {

    private static final long serialVersionUID = 1234567890L;


    public ArrayList<Emploee> aNewCompany;


    public ArrayList<Emploee> getaNewCompany() {
        return aNewCompany;
    }

    public void setaNewCompany(ArrayList<Emploee> aNewCompany) {
        this.aNewCompany = aNewCompany;
    }

}
