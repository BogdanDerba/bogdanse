package com.profit.bogdan.taskForEmploee;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by profit on 23.07.17.
 */
public class Emploee  implements Serializable{

    public String nameFirst;
    public String nameSecond;
    public String status;
    private int accountNumber;

    public Emploee(String nameFirst, String nameSecond, String status, int accountNumber) {
        this.nameFirst = nameFirst;
        this.nameSecond = nameSecond;
        this.status = status;
        this.accountNumber = accountNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public String getNameSecond() {
        return nameSecond;
    }

    public void setNameSecond(String nameSecond) {
        this.nameSecond = nameSecond;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException{
        nameFirst = aInputStream.readUTF();
        nameSecond = aInputStream.readUTF();
        status = aInputStream.readUTF();
        accountNumber = aInputStream.readInt();

    }
    private void writeObject(ObjectOutputStream aOutputStream) throws IOException {
        aOutputStream.writeUTF(nameFirst);
        aOutputStream.writeUTF(nameSecond);
        aOutputStream.writeUTF(status);
        aOutputStream.writeInt(accountNumber);

    }

    @Override
    public String toString() {
        return "Emploee{" +
                "nameFirst='" + nameFirst + '\'' +
                ", nameSecond='" + nameSecond + '\'' +
                ", status='" + status + '\'' +
                ", accountNumber=" + accountNumber +
                '}';
    }
}


