package com.profit.bogdan.taskForEmploee;

import java.io.Serializable;

/**
 * Created by profit on 23.07.17.
 */
public class emploee implements Serializable {


    public String nameFirst;
    public String nameSecond;
    public String status;

    public emploee(String nameFirst, String nameSecond, String status) {
        this.nameFirst = nameFirst;
        this.nameSecond = nameSecond;
        this.status = status;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public String getNameSecond() {
        return nameSecond;
    }

    public void setNameSecond(String nameSecond) {
        this.nameSecond = nameSecond;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "emploee{" +
                "nameFirst='" + nameFirst + '\'' +
                ", nameSecond='" + nameSecond + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
