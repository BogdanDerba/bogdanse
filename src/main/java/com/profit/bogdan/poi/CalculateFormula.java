package com.profit.bogdan.poi;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by profit on 16.07.17.
 */
public class CalculateFormula {

    public static void main(String[] args) {

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("Calculate Simple Interest");

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Principal");
        header.createCell(1).setCellValue("Interest");
        header.createCell(2).setCellValue("Time");
        header.createCell(3).setCellValue("OUTPUT(P * r * t)");

        Row dataRow = sheet.createRow(1);
        dataRow.createCell(0).setCellValue(1000);
        dataRow.createCell(1).setCellValue(12.00);
        dataRow.createCell(2).setCellValue(6);
        dataRow.createCell(3).setCellValue("A2 * B2 * C2");

        try(FileOutputStream out = new FileOutputStream(new File("Writesheet2.xlsx"))){
            workbook.write(out);
            out.flush();
            System.out.println("");
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }


    }
}
