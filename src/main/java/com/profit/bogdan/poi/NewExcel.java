package com.profit.bogdan.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by profit on 16.07.17.
 */
public class NewExcel {
    public static void main(String[] args) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet spreadsheet = workbook.createSheet(" Emploee Info");


        XSSFRow row;


        Map<String, Object[]> empinfo = new TreeMap<>();

        empinfo.put("1", new Object[]{"EMP ID", "EMP NAME", "DESIGNATION"});
        empinfo.put("2", new Object[]{"tp01", "Golap", "Teh manager"});
        empinfo.put("3", new Object[]{"tp02", "Manisha", "Proof Reader"});
        empinfo.put("4", new Object[]{"tp03", "Mathon", "Teh writer"});
        empinfo.put("5", new Object[]{"tp04", "Satish", "Teh writer"});
        empinfo.put("6", new Object[]{"tp05", "Kristi", "Teh writer"});

        Set<String> keyid = empinfo.keySet();

        int rowid = 0;
        for (String key : keyid) {
            row = spreadsheet.createRow(rowid++);
            Object[] object = empinfo.get(key);

            int cellid = 0;
            for (Object obj : object) {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String) obj);
            }
        }


        try (FileOutputStream out = new FileOutputStream(new File("Writesheet.xlsx"))) {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();

        }

    }
}
