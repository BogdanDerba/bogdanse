package com.profit.bogdan.inheritance;

/**
 * Created by profit on 11.06.17.
 */
public class Angle extends Figure {
    /**
     *
     */
    private double height;
    /**
     *
     */
    private double area = 2.3;

    public Angle(double side, double radius, double area) {
        super(side, radius, area);
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public void setArea(double area) {
        this.area = area;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return " Angle: subclass of " + super.toString() + " Area = " + (getArea() * 2);
    }
}
