package com.profit.bogdan.inheritance;

/**
 * Created by profit on 11.06.17.
 */
public class Cylindre extends Circle {
    /**
     *
     */
    private double height;


    //
    public Cylindre() {
        //public Cylindre() {
        super();
        height = 1.0;

    }

    // public Cylindre(double height) {
    //     super();
    //   this.height = height;
    // }
    public Cylindre(double radius, double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }


    public double getVolume() {
        return getArea() * height;
    }

    @Override
    public String toString() {
        return " Cylindre: subclass of " + super.toString()
                + " height = " + height;
    }
}
