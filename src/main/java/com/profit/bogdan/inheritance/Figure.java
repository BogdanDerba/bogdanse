package com.profit.bogdan.inheritance;

/**
 * Created by profit on 11.06.17.
 */
public class Figure {
    /**
     *
     */
    private double side;
    /**
     *
     */
    private double radius;
    /**
     *
     */
    private double area;
    /**
     *
     */
    private String toString;

    public Figure(double side, double radius, double area) {
        this.side = side;
        this.radius = radius;
        this.area = area;
    }

    public Figure() {

    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

