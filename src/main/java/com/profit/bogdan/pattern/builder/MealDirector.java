package com.profit.bogdan.pattern.builder;

/**
 * Created by profit on 15.07.17.
 */
public class MealDirector {
    private MealBuilder mealBuilder = null;

    public MealDirector(MealBuilder mealBuilder) {
        this.mealBuilder = mealBuilder;
    }

    public MealDirector constractMeal(){

        mealBuilder.buildDrink();
        mealBuilder.buildMainCourse();
        mealBuilder.buildSide();
        return this;
    }

    public Meal getMeal(){
        return mealBuilder.getMeal();
    }
}
