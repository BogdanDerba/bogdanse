package com.profit.bogdan.pattern.abstractFactory;

import com.profit.bogdan.pattern.factory.Animal;
import com.profit.bogdan.pattern.factory.Snake;
import com.profit.bogdan.pattern.factory.Tyrannosaurus;

/**
 * Created by profit on 15.07.17.
 */
public class ReptileFactory extends SpeciesFactory {
    @Override
    public Animal getAnimal(String type) {
        if ("snake".equals(type)) {
            return new Snake();

        } else {
            return new Tyrannosaurus();
        }
    }
}
