package com.profit.bogdan.pattern.abstractFactory;

import com.profit.bogdan.pattern.factory.Animal;
import com.profit.bogdan.pattern.factory.Cat;
import com.profit.bogdan.pattern.factory.Dog;

/**
 * Created by profit on 15.07.17.
 */
public class MammalFactory extends SpeciesFactory {
    @Override
    public Animal getAnimal(String type) {
        if ("dog".equals(type)) {
            return new Dog();
        } else {

            return new Cat();
        }
    }
}
