package com.profit.bogdan.pattern.abstractFactory;

import com.profit.bogdan.pattern.factory.Animal;

/**
 * Created by profit on 15.07.17.
 */
public abstract class SpeciesFactory {
    public abstract Animal getAnimal(String type);
}
