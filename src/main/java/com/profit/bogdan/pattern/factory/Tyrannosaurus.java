package com.profit.bogdan.pattern.factory;

/**
 * Created by profit on 15.07.17.
 */
public class Tyrannosaurus extends Animal {
    @Override
    public String makeSound() {
        return "Roar";
    }
}
