package com.profit.bogdan.pattern.factory;

/**
 * Created by profit on 15.07.17.
 */
public class AnimalFactory {

    public Animal getAnimal(String type){
        if ("canine".equals(type)){
            return new Dog();
        }
        else if ("fenine".equals(type)){
            return new Cat();
        } else {
            return new Cat();
        }
    }
}
