package com.profit.bogdan.pattern.factory;

/**
 * Created by profit on 15.07.17.
 */
public class Snake extends Animal{
    @Override
    public String makeSound() {
        return "Hiss";
    }
}
