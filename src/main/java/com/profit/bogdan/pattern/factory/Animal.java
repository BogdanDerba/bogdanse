package com.profit.bogdan.pattern.factory;

/**
 * Created by profit on 15.07.17.
 */
public abstract class Animal {
    public abstract String makeSound();
}
