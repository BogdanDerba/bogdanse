package com.profit.bogdan.pattern.factory;

import com.profit.bogdan.reflection.AnnotationTest;

/**
 * Created by profit on 15.07.17.
 */
public class Cat extends Animal{
    @Override
    public String makeSound() {
        return "Meow!";
    }
}
