package com.profit.bogdan.pattern.factory;

import java.util.List;

/**
 * Created by profit on 15.07.17.
 */
public final class Student {
    /**
     *
     */
    private String name;
    /**
     *
     */
    private int age;
    /**
     *
     */
    private List<String> language;

    /**
     *
     */
    public static class Builder {
        /**
         *
         */
        private String name;
        /**
         *
         */
        private int age;
        /**
         *
         */
        private List<String> language;


        /**
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * @param age
         * @return
         */
        public Builder age(int age) {
            this.age = age;
            return this;
        }

        /**
         * @param language
         * @return
         */
        public Builder language(List<String> language) {
            this.language = language;
            return this;
        }

        /**
         * @return
         */
        public Student build() {
            return new Student(this);
        }
    }

    private Student(Builder builder) {
        name = builder.name;
        age = builder.age;
        language = builder.language;
    }

    @Override
    public String toString() {
        return "Student{"
                + "name = " + name + '\''
                + ", age = " + age + '\''
                + ", language = " + language
                + '}';
    }
}
