package com.profit.bogdan.pattern.prototype;

/**
 * Created by profit on 15.07.17.
 */
public class Dog  implements Prototype{
    /**
     *
     */
    String sound;

    public Dog(String sound) {
        this.sound = sound;
    }

    @Override
    public Prototype doClone() {
        return new Dog(sound);
    }

    public String toString(){
        return "This dog says " + sound;
    }
}
