package com.profit.bogdan.pattern.prototype;

/**
 * Created by profit on 15.07.17.
 */
public interface Prototype {

   Prototype doClone();
}
