package com.profit.bogdan.encapsulation;

import java.util.Date;

/**
 * Created by profit on 17.06.17.
 */
public class User {
    /**
     *
     */
    private String firstName;
    /**
     *
     */
    private String lastName;
    /**
     *
     */
    private String loginId;
    /**
     *
     */
    private String password;
    /**
     *
     */
    private String email;
    /**
     *
     */
    private Date registered;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginId() {
        return loginId;
    }

     public void setLoginId(String loginId) {
         this.loginId = loginId;
     }

    /**
     *
     * @param //loginId
     */
    //public void setLoginId(String loginId) {
    //  if (loginId.matches("^[a-zA-z][a-zA-Z0-9]8$")) {
    //        this.loginId = loginId;
    //  } else {
    //        System.out.println();
    //    }
    //}

    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        if (password.length() < 6) {
            System.out.println("Password is too short");
        } else {

            this.password = password;
        }
    }

    //public void setPassword(String password) {
    //   if (password == null || password.length() < 6) {
    //     System.err.println("Password is too short");
    //} else {

    //            this.password = password;
    //      }
    // }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }
}
