package com.profit.bogdan.reports;

import net.sf.dynamicreports.adhoc.AdhocManager;
import net.sf.dynamicreports.adhoc.configuration.AdhocColumn;
import net.sf.dynamicreports.adhoc.configuration.AdhocConfiguration;
import net.sf.dynamicreports.adhoc.configuration.AdhocReport;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by profit on 24.06.17.
 */
public class SimpleReport {
    public SimpleReport() {
        build();
    }

    private void build() {
        AdhocConfiguration configuration = new AdhocConfiguration();
        AdhocReport report = new AdhocReport();
        configuration.setReport(report);

        AdhocColumn column = new AdhocColumn();
        column.setName("item");
        report.addColumn(column);


        column = new AdhocColumn();
        column.setName("quantity");
        report.addColumn(column);

        try {
            AdhocManager.saveConfiguration(configuration,
                    new FileOutputStream("/home/profit/IdeaProjects/bogdan/reports_configuration.doc"));
            AdhocConfiguration loadedConfiguration = AdhocManager.loadConfiguration(new FileInputStream(
                    "/home/profit/IdeaProjects/bogdan/reports_configuration.doc"));

            JasperReportBuilder reportBuilder = AdhocManager.createReport(configuration.getReport());
            reportBuilder.setDataSource(createDataSource());
            reportBuilder.toPdf(new FileOutputStream(new File(
                    "/home/profit/IdeaProjects/bogdan/reports_configuration.pdf")));
            reportBuilder.show();
        } catch (DRException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private JRDataSource createDataSource() {

        DRDataSource dataSource = new DRDataSource("item", "arderdate", "quantity", "unitprice");
        for (int i = 0; i < 20; i++) {
            dataSource.add("Book", new Date(), (int) (Math.random()
                    * 10000) + 1, new BigDecimal(Math.random() * 100 + 1));

        }
        return dataSource;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        new SimpleReport();
    }


}
