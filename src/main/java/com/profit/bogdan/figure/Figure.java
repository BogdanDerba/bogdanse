package com.profit.bogdan.figure;

/**
 * Created by profit on 11.06.17.
 */
public class Figure {
    /**
     * area;
     */
    private double area;

    @Override
    public String toString() {
        return super.toString();
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public Figure(double area) {

        this.area = area;

    }
}
