package com.profit.bogdan.polymorphism;

/**
 * Created by profit on 17.06.17.
 */
abstract  class StaffMember {
    /**
     *
     */
    private String name;
    /**
     *
     */
    private String address;
    /**
     *
     */
    private String phone;

    /**
     * Set up a staff member using the specified inform;
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    protected StaffMember(String eName, String eAddress, String ePhone) {

        name = eName;
        address = eAddress;
        phone = ePhone;
    }

    /**
     * Return a string including the basic emploee inform;
     *
     * @return
     */
    @Override
    public String toString() {
        String result = "Name: " + name + "\n";
        result += "Address: " + address + "\n";
        result += "Phone: " + phone;
        return result;
    }

    /**
     * Derived classes must define the pay method for each tupe of emploee;
     *
     * @return
     */
    public abstract double pay();
}
