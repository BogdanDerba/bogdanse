package com.profit.bogdan.polymorphism;

import java.util.Comparator;

/**
 * Created by profit on 17.06.17.
 */
public class Emploee extends StaffMember {
    /**
     *
     */
    protected static Comparator<? super Emploee> nameComparator;
    /**
     *
     */
    protected String socialSecurityNumber;
    /**
     *
     */
    protected double payRate;

    /**
     * Set up an emploee with specified infirmation;
     *  @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Emploee(String  eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone);
        socialSecurityNumber = socSecNumber;
        payRate = rate;
    }

    /**
     * Returns information about an emploee as a string;
     *
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nSocial Security Number: " + socialSecurityNumber;
        return result;
    }

    /**
     * Returns the pay rate for this emploee;
     *
     * @return
     */
    @Override
    public double pay() {
        return payRate;
    }
}
