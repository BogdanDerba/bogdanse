package com.profit.bogdan.polymorphism;

/**
 * Created by profit on 17.06.17.
 */
public class Hourly extends Emploee {

    /**
     *
     */
    private int hoursWorked;

    /**
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Hourly(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone, socSecNumber, rate);
        hoursWorked = 0;
    }

    /**
     * @param moreHours
     */
    public void addHours(int moreHours) {
        hoursWorked += moreHours;
    }

    /**
     * @return
     */
    @Override
    public double pay() {
        double payment = payRate * hoursWorked;
        hoursWorked = 0;
        return payment;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return result;
    }
}
