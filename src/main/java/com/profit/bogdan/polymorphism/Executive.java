package com.profit.bogdan.polymorphism;

/**
 * Created by profit on 17.06.17.
 */
public class Executive extends Emploee {
    /**
     *
     */
    private double bonus;

    /**
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Executive(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone, socSecNumber, rate);
        bonus = 0;
    }

    /**
     * @param execBonus
     */
    public void awardBonus(double execBonus) {
        bonus = execBonus;
    }

    /**
     * @return
     */
    @Override
    public double pay() {
        double payment = super.pay() + bonus;
        bonus = 0;
        return payment;
    }
}
