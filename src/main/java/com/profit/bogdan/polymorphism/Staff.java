package com.profit.bogdan.polymorphism;

import java.util.ArrayList;

/**
 * Created by profit on 17.06.17.
 */
public class Staff {
    /**
     *
     */
    private ArrayList<StaffMember> staffList;

    /**
     * Sets up the list of staff members;
     */
    public Staff() {
        staffList = new ArrayList<>();
        staffList.add(new Executive("Sam", "234-234", "234-7-69-67-75", "234-2345-3", 324.235));
        staffList.add(new Emploee("Sasha", "234-234", "278-7-78-7345", "234-2345-3", 32.235));
        staffList.add(new Emploee("Igor", "23-4234", "2345", "234-2345-3", 23.235));
        staffList.add(new Hourly("Olga", "2342-34", "234-89-789-795", "234-2345-3", 24.235));
        staffList.add(new Volunteer("Andrey", "234234", "2-379-4789-5"));
        staffList.add(new Volunteer("Carl", "234234", "239-789-79-45"));

        ((Executive) staffList.get(0)).awardBonus(500.00);
        ((Hourly) staffList.get(3)).addHours(40);

    }

    /**
     * Pays all staff member;
     */
    public void payDay() {

        double amount;
        for (int count = 0; count < staffList.size(); count++) {
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();
            // polimorphic
            if (amount == 0.0) {
                System.out.println("Thanks!");
                System.out.println("-------------------------------------------------");
            } else {
                System.out.println("Paid: " + amount);
                System.out.println("-------------------------------------------------");
            }


        }
    }
}

